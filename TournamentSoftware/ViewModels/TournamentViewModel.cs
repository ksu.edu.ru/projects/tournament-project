﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using TournamentSoftware.Commands;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.Windows.BattleProtocol;
using TournamentSoftware.Windows.Statistic;

namespace TournamentSoftware.ViewModels
{
    class TournamentViewModel : NotifyPropertyChangedObject
    {
        Group selectedGroup;
        Subgroup selectedSubgroup;
        BattleProtocol selectedBattleProtocol;
        List<SubgroupMember> subgroupMembers;
        int countOfPhases;

        bool canFinishTournament;
        readonly Club emptyClub;
        readonly City emptyCity;

        public TournamentViewModel()
        {
            emptyCity = new City { Name = "Без города" };
            emptyClub = new Club { Name = "Без клуба", City = emptyCity };
            LoadData();
            AddPhasesCommand = new LambdaCommand(OnAddPhasesCommandExecuted, CanAddPhasesCommandExecute);
            AddPhaseCommand = new LambdaCommand(OnAddPhaseCommandExecuted, CanAddPhaseCommandExecute);
            DeletePhaseCommand = new LambdaCommand(OnDeletePhaseCommandExecuted, CanDeletePhaseCommandExecute);
            OpenBattleProtocolCommand = new LambdaCommand(OnOpenBattleProtocolCommandExecuted, CanOpenBattleProtocolCommandExecute);
            OpenSubgroupStatisticCommand = new LambdaCommand(OnOpenSubgroupStatisticExecuted, CanOpenSubgroupStatisticExecute);
            ToFinalSubgroupCommand = new LambdaCommand(OnToFinalSubgroupCommandExecuted, CanToFinalSubgroupCommandExecute);
            FinishTournamentCommand = new LambdaCommand(OnFinishTournamentCommandExecuted, CanFinishTournamentCommandExecute);
            ToSubgroupsFormationCommand = new LambdaCommand(OnToSubgroupsFormationCommandExecuted, CanToSubgroupsFormationCommandExecute);
            CanFinishTournament = IsTournamentFinished();
        }

        #region Properties

        public List<FightSystem> FightSystems { get; private set; }

        public List<Group> Groups { get; set; }

        public Group SelectedGroup
        {
            get => selectedGroup;
            set => Set(ref selectedGroup, value);
        }

        bool SelectedGroupIncludeLeaderRule => SelectedGroup.GroupRules.Exists(gr => gr.Name == "Правило посевных бойцов");

        bool SelectedGroupIncludeClubRule => SelectedGroup.GroupRules.Exists(gr => gr.Name == "Правило одноклубников");

        bool SelectedGroupIncludeCityRule => SelectedGroup.GroupRules.Exists(gr => gr.Name == "Правило города");

        public Subgroup SelectedSubgroup
        {
            get => selectedSubgroup;
            set
            {
                Set(ref selectedSubgroup, value);
                CountOfPhases = value?.Phases.Count ?? 0;
                MainViewModel.SelectedSubgroupId = value?.Id ?? 0;
                SelectedBattleProtocol = null;
            }
        }

        public List<Subgroup> Subgroups { get; set; }

        public BattleProtocol SelectedBattleProtocol
        {
            get => selectedBattleProtocol;
            set
            {
                Set(ref selectedBattleProtocol, value);
                MainViewModel.SelectedBattleProtocolId = value?.Id ?? 0;
            }
        }

        public bool CanFinishTournament
        {
            get => canFinishTournament;
            set => Set(ref canFinishTournament, value);
        }

        public int CountOfPhases
        {
            get => countOfPhases;
            set => Set(ref countOfPhases, value);
        }

        #endregion

        #region Commands

        #region AddPhasesCommand

        public ICommand AddPhasesCommand { get; }

        bool CanAddPhasesCommandExecute(object parameter) => SelectedGroup != null && SelectedSubgroup != null && SelectedSubgroup.FightSystem != null && SelectedSubgroup.Phases.Count == 0 && ((SelectedSubgroup.FightSystem.Name == "Круговая" && CountOfPhases > 0) || SelectedSubgroup.FightSystem.Name != "Круговая");
        
        void OnAddPhasesCommandExecuted(object parameter)
        {
            SaveSubgroupFightSystem();
            if (SelectedSubgroup.Name == "Финал")
                UpdateNumber();
            AddPhases(SelectedSubgroup.FightSystem.Name == "Круговая" ? CountOfPhases : 1);
        }

        #endregion

        #region AddPhaseCommand

        public ICommand AddPhaseCommand { get; }

        bool CanAddPhaseCommandExecute(object parameter) => SelectedSubgroup?.FightSystem?.Name == "Круговая" && SelectedSubgroup.Phases.Count != 0;

        void OnAddPhaseCommandExecuted(object parameter) => AddPhases(1);

        #endregion

        #region DeletePhaseCommand

        public ICommand DeletePhaseCommand { get; }

        bool CanDeletePhaseCommandExecute(object parameter) => SelectedSubgroup?.FightSystem?.Name == "Круговая" && SelectedSubgroup.Phases.Count > 1 && selectedSubgroup.Phases.Last().BattleProtocols.All(bp => bp.Fighters.All(f => f.Result == FighterResultType.Nope));

        void OnDeletePhaseCommandExecuted(object parameter) => DeletePhases(1);

        #endregion

        #region  OpenSubgroupStatistic

        public ICommand OpenSubgroupStatisticCommand { get; }

        bool CanOpenSubgroupStatisticExecute(object parameter) => SelectedSubgroup != null;

        void OnOpenSubgroupStatisticExecuted(object parameter)
        {
            StatisticWindow statisticWindow = new StatisticWindow();
            if ((bool)statisticWindow.ShowDialog())
                UpdateData();
        }

        #endregion

        #region OpenBattleProtocolCommand

        public ICommand OpenBattleProtocolCommand { get; }

        bool CanOpenBattleProtocolCommandExecute(object parameter) => SelectedBattleProtocol != null && SelectedBattleProtocol.Id != 0;

        void OnOpenBattleProtocolCommandExecuted(object parameter)
        {
            BattleProtocolWindow battleProtocolWindow = new BattleProtocolWindow();
            if ((bool)battleProtocolWindow.ShowDialog())
                BattleProtocolSaved(SelectedBattleProtocol);
            CanFinishTournament = IsTournamentFinished();
        }

        #endregion

        #region ToFinalSubgroupCommand

        public ICommand ToFinalSubgroupCommand { get; }

        bool CanToFinalSubgroupCommandExecute(object parameter) => SelectedGroup != null && SelectedGroup.IsFinalEnabled && !SelectedGroup.IsFinalCreated && SelectedGroup.IsFightsOver;

        void OnToFinalSubgroupCommandExecuted(object parameter)
        {
            SelectedGroup.IsFinalCreated = true;
            Subgroup finalSubgroup = Subgroups.First(s => s.GroupId == SelectedGroup.Id && s.Name == "Финал");
            if (!SelectedGroup.Subgroups.Contains(finalSubgroup))
                SelectedGroup.Subgroups.Add(finalSubgroup);
            SelectedSubgroup = finalSubgroup;
        }

        #endregion

        #region FinishTournamentCommand

        public ICommand FinishTournamentCommand { get; }

        bool CanFinishTournamentCommandExecute(object parameter) => CanFinishTournament;

        void OnFinishTournamentCommandExecuted(object parameter)
        {
            if(MessageBox.Show("Завершить турнир", "", MessageBoxButton.YesNo) == MessageBoxResult.Yes)
            {
                FinishTournament();
                MainViewModel.SwitchPage(MainPageType.Start);
            }
        }

        #endregion

        #region ToSubgroupsFormationCommand

        public ICommand ToSubgroupsFormationCommand { get; }

        bool CanToSubgroupsFormationCommandExecute(object parameter) => true;

        void OnToSubgroupsFormationCommandExecuted(object parameter) => MainViewModel.SwitchPage(MainPageType.SubgroupsFormation);

        #endregion

        #endregion

        void SaveSubgroupFightSystem()
        {
            if (SelectedSubgroup?.FightSystem == null)
                return;
            DataBaseConnection.SQLiteConnection.Update(SelectedSubgroup);
        }

        void BattleProtocolSaved(BattleProtocol battleProtocol)
        {
            DataBaseConnection.SQLiteConnection.Query<Fighter>("SELECT * FROM Fighter WHERE battle_protocol_id=?", battleProtocol.Id).ForEach(f => battleProtocol.Fighters.Find(bpf => bpf.Id == f.Id).TotalScore = f.TotalScore);
            List<Fighter> fighters = battleProtocol.Fighters;
            Fighter fighterWithMaxTotalScore = fighters.First(fighter => fighter.TotalScore == fighters.Max(f => f.TotalScore));

            fighters.ForEach(f =>
            {
                if (f.Equals(fighterWithMaxTotalScore))
                    f.Result = FighterResultType.Winner;
                else
                    f.Result = FighterResultType.Defeat;
            });

            if (IsAllProtocolsComplete(SelectedSubgroup))
                if (SelectedSubgroup.FightSystem.Name == "На вылет" && CanCreatePhase(1) || SelectedSubgroup.FightSystem.Name == "До двух поражений" && CanCreatePhase(2))
                    AddPhases(1);
                else
                {
                    SelectedGroup.IsFightsOver = SelectedGroup.Subgroups.All(s => IsAllProtocolsComplete(s));
                    SelectedGroup.IsFinalCreated = SelectedSubgroup.Name == "Финал";
                }
        }

        bool IsAllProtocolsComplete(Subgroup subgroup) => subgroup.Phases.Count > 0 && subgroup.Phases.Last().BattleProtocols.ToList().All(battleProtocol => battleProtocol.Fighters.All(fighter => fighter.Result != FighterResultType.Nope));

        bool CanCreatePhase(int numberOfDefeatsForRelegation) => GetMembersForNewPhase(numberOfDefeatsForRelegation).Count > 1;

        void DeletePhases(int countOfPhases)
        {
            for (int i = selectedSubgroup.Phases.Count - 1; i >= countOfPhases; i--)
            {
                Phase phase = selectedSubgroup.Phases[i];
                DataBaseConnection.SQLiteConnection.Execute("DELETE FROM Fighter WHERE battle_protocol_id IN (SELECT id FROM BattleProtocol WHERE phase_id=?);", phase.Id);
                DataBaseConnection.SQLiteConnection.Execute("DELETE FROM BattleProtocol WHERE phase_id=?;", phase.Id);
                DataBaseConnection.SQLiteConnection.Execute("DELETE FROM Phase WHERE id=?;", phase.Id);
                selectedSubgroup.Phases.Remove(phase);
            }
        }

        void AddPhases(int countOfPhases)
        {
            for (int i = 0; i < countOfPhases; i++)
            {
                Phase phase = new Phase { Number = SelectedSubgroup.Phases.Count + 1, SubgroupId = SelectedSubgroup.Id };
                DataBaseConnection.SQLiteConnection.Insert(phase);
                phase.BattleProtocols = new ObservableCollection<BattleProtocol>();
                FormatePhase(phase);
                SelectedSubgroup.Phases.Add(phase);
            }
            CountOfPhases = SelectedSubgroup.Phases.Count;
        }

        void FormatePhase(Phase newPhase)
        {
            List<List<SubgroupMember>> subgroupMemberPairs = new List<List<SubgroupMember>>();
            switch (SelectedSubgroup.FightSystem.Name)
            {
                case "Круговая":
                    List<SubgroupMember> phaseSubgroupMembers = SelectedSubgroup.Members.ToList().ConvertAll(m => subgroupMembers.First(sm => sm.SubgroupId == SelectedSubgroup.Id && sm.MemberId == m.Id));
                    for (int i = 0; i < phaseSubgroupMembers.Count; i++)
                        for (int j = i + 1; j < phaseSubgroupMembers.Count; j++)
                            subgroupMemberPairs.Add(new List<SubgroupMember> { phaseSubgroupMembers[i], phaseSubgroupMembers[j] });
                    break;
                case "На вылет":
                    subgroupMemberPairs = FightersDistributionInPairsByWinCount(GetMembersForNewPhase(1));
                    break;
                case "До двух поражений":
                    subgroupMemberPairs = FightersDistributionInPairsByWinCount(GetMembersForNewPhase(2));
                    break;
                default:
                    throw new Exception("Неизвестная система боя.");
            }
            subgroupMemberPairs.Sort((first, second) => PrioritizePair(first).CompareTo(PrioritizePair(second)));
            subgroupMemberPairs.ForEach(subgroupMembers =>
            {
                if (subgroupMembers.Count == 2)
                    CreateBattleProtocol(newPhase, subgroupMembers);
                else if (subgroupMembers.Count == 1)
                {
                    newPhase.ExtraMemberSubgroup = subgroupMembers.First();
                    SavePhaseExtraMember(newPhase, newPhase.ExtraMemberSubgroup);
                    CreateBattleProtocolForLoneFighter(newPhase, newPhase.ExtraMemberSubgroup);
                }
            });
        }

        List<SubgroupMember> GetMembersForNewPhase(int numberOfDefeatsForRelegation)
        {
            List<SubgroupMember> newPhaseSubgroupMembers;
            if (SelectedSubgroup.Phases.Count > 0)
            {
                newPhaseSubgroupMembers = new List<SubgroupMember>();
                List<Fighter> phaseFighters = new List<Fighter>();
                SelectedSubgroup.Phases.Last().BattleProtocols.ToList().ForEach(battleProtocol => phaseFighters.AddRange(battleProtocol.Fighters.FindAll(f => !HasFighterLost(f, numberOfDefeatsForRelegation))));
                newPhaseSubgroupMembers.AddRange(phaseFighters.ConvertAll(f => f.SubgroupMember));
            }
            else
                newPhaseSubgroupMembers = SelectedSubgroup.Members.ToList().ConvertAll(m => subgroupMembers.First(sm => sm.MemberId == m.Id && sm.SubgroupId == SelectedSubgroup.Id));
            return newPhaseSubgroupMembers;
        }

        bool HasFighterLost(Fighter fighter, int numberOfDefeatsForRelegation)
        {
            if (fighter.SubgroupMemberId == 0 || fighter.SubgroupMember.IsDisqualified)
                return true;
            int defeatsCount = 0;
            for (int i = SelectedSubgroup.Phases.Count - 1; i >= 0 && defeatsCount < numberOfDefeatsForRelegation; i--)
                SelectedSubgroup.Phases[i].BattleProtocols.ToList().ForEach(bp =>
                {
                    if (bp.Fighters.Exists(f => f.Result == FighterResultType.Defeat && f.SubgroupMemberId == fighter.SubgroupMemberId))
                        defeatsCount++;
                });
            return defeatsCount >= numberOfDefeatsForRelegation;
        }

        List<List<SubgroupMember>> FightersDistributionInPairsByWinCount(List<SubgroupMember> phaseSubgroupMembers)
        {
            Dictionary<int, List<SubgroupMember>> numberLosersToMembers = new Dictionary<int, List<SubgroupMember>>();
            List<List<SubgroupMember>> subgroupMemberPairs = new List<List<SubgroupMember>>();

            if (SelectedSubgroup.Phases.Count > 0)
            {
                List<Fighter> defeatFighters = new List<Fighter>();

                SelectedSubgroup.Phases.ToList().ForEach(p => p.BattleProtocols.ToList().ForEach(bp => defeatFighters.AddRange(bp.Fighters.FindAll(f => f.Result == FighterResultType.Defeat && f.SubgroupMemberId != 0))));
                phaseSubgroupMembers.ForEach(sm =>
                {
                    int defeatCount = defeatFighters.Count(f => f.SubgroupMemberId == sm.Id && !sm.IsDisqualified);
                    if (numberLosersToMembers.TryGetValue(defeatCount, out List<SubgroupMember> lossesMembers))
                        lossesMembers.Add(sm);
                    else
                        numberLosersToMembers.Add(defeatCount, new List<SubgroupMember> { sm });
                });

                List<KeyValuePair<int, List<SubgroupMember>>> numberLossesToMembersList = numberLosersToMembers.ToList();
                numberLossesToMembersList.Sort((first, second) => first.Key.CompareTo(second.Key));

                if (phaseSubgroupMembers.Exists(sm => sm.MemberId == SelectedSubgroup.Phases.Last().ExtraMemberSubgroup?.Member.Id))
                {
                    KeyValuePair<int, List<SubgroupMember>> loserMembersWithExtraMember = numberLossesToMembersList.First(p => p.Value.Exists(sm => sm.MemberId == SelectedSubgroup.Phases.Last().ExtraMemberSubgroup?.Member.Id));
                    numberLossesToMembersList.Remove(loserMembersWithExtraMember);
                    numberLossesToMembersList.Insert(0, loserMembersWithExtraMember);
                }
                numberLosersToMembers = numberLossesToMembersList.ToDictionary(p => p.Key, p => p.Value);
            }
            else
                numberLosersToMembers.Add(0, phaseSubgroupMembers);

            numberLosersToMembers.Keys.ToList().ForEach(k =>
            {
                Member extraMember;
                subgroupMemberPairs.AddRange(FightersDistributionInPairsBySubgroupRules(numberLosersToMembers[k], out extraMember));
                if (extraMember != null)
                    if (numberLosersToMembers.Keys.Last() != k)
                        numberLosersToMembers.SkipWhile(kvp => kvp.Key != k).First(ky => ky.Key != k).Value.Add(phaseSubgroupMembers.First(sm => sm.MemberId == extraMember.Id));
                    else
                        subgroupMemberPairs.Add(new List<SubgroupMember> { phaseSubgroupMembers.First(sm => sm.MemberId == extraMember.Id) });
            });

            return subgroupMemberPairs;
        }

        bool HasSubgroupFinalists(List<Member> members, List<Member> finalSubgroupMembers) => members.Exists(m => finalSubgroupMembers.Exists(sm => sm.Id == m.Id));
        
        List<List<SubgroupMember>> FightersDistributionInPairsBySubgroupRules(List<SubgroupMember> subgroupMember, out Member extraMember)
        {
            Member extraMemberOfLastPhase = SelectedSubgroup.Phases.Count > 0 && subgroupMember.Exists(sm => sm.Id == SelectedSubgroup.Phases.Last().ExtraMemberSubgroup?.Id) ? SelectedSubgroup.Phases.Last().ExtraMemberSubgroup?.Member : null;
            extraMember = null;
            List<Member> members = subgroupMember.ConvertAll(sm => sm.Member);
            Dictionary<int, List<int>> membersToOpponents = new Dictionary<int, List<int>>();
            List<List<int>> battleProtocolsToMembers = new List<List<int>>();
            List<Member> membersWithoutPair = new List<Member>();

            members.ForEach(m => membersToOpponents.Add(m.Id, members.FindAll(opp => m.Id != opp.Id && (!SelectedGroupIncludeLeaderRule || !m.Leader || !opp.Leader) && (!SelectedGroupIncludeCityRule || opp.City.Id == 0 || m.City.Id != opp.City.Id) && (!SelectedGroupIncludeClubRule || opp.Club.Id == 0 || m.Club.Id != opp.Club.Id)).ConvertAll(opp => opp.Id)));

            if (membersToOpponents.Count > 1 && extraMemberOfLastPhase != null && membersToOpponents.TryGetValue(extraMemberOfLastPhase.Id, out List<int> extraMemberOpponents) && extraMemberOpponents.Count == 0)
            {
                int opponentId = membersToOpponents.First(p => p.Key != extraMemberOfLastPhase.Id).Key;
                membersToOpponents[opponentId].Add(extraMemberOfLastPhase.Id);
                extraMemberOpponents.Add(opponentId);
            }

            while (membersToOpponents.Count != 0)
            {
                int memberWithMinOpponentsCount = membersToOpponents.Min(m =>  m.Value.Count);
                KeyValuePair<int, List<int>> memberToOpponents;

                if (extraMemberOfLastPhase != null)
                {
                    memberToOpponents = membersToOpponents.First(p => p.Key == extraMemberOfLastPhase.Id);
                    extraMemberOfLastPhase = null;
                } else
                    memberToOpponents = membersToOpponents.First(opp => opp.Value.Count == memberWithMinOpponentsCount);

                if (memberToOpponents.Value.Count == 0)
                {
                    membersWithoutPair.Add(members.First(m => m.Id == memberToOpponents.Key));
                    membersToOpponents.Remove(memberToOpponents.Key);
                } else
                {
                    int opponentWithMinOpponentsCount = memberToOpponents.Value.Min(opp => membersToOpponents[opp].Count);
                    int opponentId = memberToOpponents.Value.First(opp => membersToOpponents[opp].Count == opponentWithMinOpponentsCount);
                    battleProtocolsToMembers.Add(new List<int> { memberToOpponents.Key, opponentId });
                    membersToOpponents.Remove(memberToOpponents.Key);
                    membersToOpponents.Remove(opponentId);
                    membersToOpponents.Values.ToList().ForEach(mTO =>
                    {
                        mTO.Remove(memberToOpponents.Key);
                        mTO.Remove(opponentId);
                    });
                }
            }

            if (membersWithoutPair.Count != 0)
            {
                if (SelectedGroupIncludeLeaderRule)
                {
                    List<Member> leaders = membersWithoutPair.FindAll(m => m.Leader);
                    for (int i = 0; i < leaders.Count / 2; i++)
                    {
                        List<Member> pair = leaders.GetRange(i * 2, 2);
                        battleProtocolsToMembers.Add(pair.ConvertAll(m => m.Id));
                        membersWithoutPair.RemoveAll(m => pair.Contains(m));
                    }
                }
                int countCityClubMembers = membersWithoutPair.Count;
                for (int i = 0; i < countCityClubMembers / 2; i++)
                {
                    List<Member> pair = membersWithoutPair.GetRange(0, 2);
                    battleProtocolsToMembers.Add(pair.ConvertAll(m => m.Id));
                    membersWithoutPair.RemoveAll(m => pair.Contains(m));
                }
                if (membersWithoutPair.Count == 1)
                    extraMember = membersWithoutPair.First();
            }

            return battleProtocolsToMembers.ConvertAll(bptm => bptm.ConvertAll(id => subgroupMember.First(sm => sm.MemberId == id)));
        }

        int PrioritizePair(List<SubgroupMember> x)
        {
            if (x.Count == 1)
                return 5;

            List<Member> m = x.ConvertAll(sm => sm.Member);

            if (SelectedGroupIncludeLeaderRule && m[0].Leader && m[0].Leader == m[1].Leader)
                return 4;
            if (SelectedGroupIncludeClubRule && m[0].Club?.Id != 0 && m[0].Club?.Id == m[1].Club?.Id)
                return 1;
            if (SelectedGroupIncludeCityRule && m[0].City?.Id != 0 && m[0].City?.Id == m[1].City?.Id)
                return 2;
            return 3;
        }

        void CreateBattleProtocol(Phase phase, List<SubgroupMember> subgroupMembers)
        {
            BattleProtocol battleProtocol = new BattleProtocol { PhaseId = phase.Id, Number = SelectedSubgroup.Phases.Sum(p => p.BattleProtocols.ToList().FindAll(bp => bp.Id > 0).Count) + phase.BattleProtocols.Count + 1 };
            DataBaseConnection.SQLiteConnection.Insert(battleProtocol);

            List<Fighter> fighters = new List<Fighter>();
            subgroupMembers.ForEach(subgroupMember =>
            {
                Fighter fighter = new Fighter { BattleProtocolId = battleProtocol.Id, SubgroupMember = subgroupMember };
                DataBaseConnection.SQLiteConnection.Insert(fighter);
                fighters.Add(fighter);
            });
            battleProtocol.Fighters = fighters;
            phase.BattleProtocols.Add(battleProtocol);
        }

        void SavePhaseExtraMember(Phase phase, SubgroupMember extraSubgroupMember)
        {
            DataBaseConnection.SQLiteConnection.Update(new Phase { Id = phase.Id, SubgroupId = phase.SubgroupId, Number = phase.Number, ExtraSubgroupMemberId = extraSubgroupMember.Id });
        }

        void CreateBattleProtocolForLoneFighter(Phase newPhase, SubgroupMember extraSubgroupMember) => newPhase.BattleProtocols.Add(new BattleProtocol { Fighters = new List<Fighter> { new Fighter { SubgroupMember = extraSubgroupMember, Result = FighterResultType.Winner }, new Fighter { Result = FighterResultType.Defeat } } } );

        void LoadData()
        {
            FightSystems = DataBaseConnection.SQLiteConnection.Query<FightSystem>("SELECT * FROM FightSystem;");

            List<TournamentCategory> tournamentCategories = DataBaseConnection.SQLiteConnection.Query<TournamentCategory>("SELECT * FROM TournamentCategory WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            List<TournamentNomination> tournamentNominations = DataBaseConnection.SQLiteConnection.Query<TournamentNomination>("SELECT * FROM TournamentNomination WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            List<Category> categories = DataBaseConnection.SQLiteConnection.Query<Category>("SELECT * FROM Category WHERE id IN (SELECT category_id FROM TournamentCategory WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            List<Nomination> nominations = DataBaseConnection.SQLiteConnection.Query<Nomination>("SELECT * FROM Nomination WHERE id IN (SELECT nomination_id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id);

            List<GroupRule_Group> groupRulesPerGroups = DataBaseConnection.SQLiteConnection.Query<GroupRule_Group>("SELECT * FROM GroupRule_Group WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            List<GroupRule> groupRules = DataBaseConnection.SQLiteConnection.Query<GroupRule>("SELECT * FROM GroupRule WHERE (SELECT DISTINCT group_rule_id FROM GroupRule_Group WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);

            List<City> cities = new List<City> { emptyCity };
            List<Club> clubs = new List<Club> { emptyClub };
            cities.AddRange(DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City WHERE id IN (SELECT DISTINCT city_id FROM Member WHERE id IN (SELECT DISTINCT member_id FROM SubgroupMember WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id));
            clubs.AddRange(DataBaseConnection.SQLiteConnection.Query<Club>("SELECT * FROM Club WHERE id IN (SELECT DISTINCT club_id FROM Member WHERE id IN (SELECT DISTINCT member_id FROM SubgroupMember WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(club =>
            {
                club.City = cities.First(c => club.CityId == c.Id);
                return club;
            }));
            
            List<Member> members = DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT DISTINCT member_id FROM SubgroupMember WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(m =>
            {
                m.Club = clubs.First(c => c.Id == m.ClubId);
                m.City = cities.First(c => c.Id == m.CityId);
                return m;
            });

            subgroupMembers = DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(sm =>
            {
                sm.Member = members.First(m => m.Id == sm.MemberId);
                return sm;
            });

            List<Phase> phases = DataBaseConnection.SQLiteConnection.Query<Phase>("SELECT * FROM Phase WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            List<BattleProtocol> battleProtocols = DataBaseConnection.SQLiteConnection.Query<BattleProtocol>("SELECT * FROM BattleProtocol WHERE phase_id IN (SELECT id FROM Phase WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            List<Fighter> fighters = DataBaseConnection.SQLiteConnection.Query<Fighter>("SELECT * FROM Fighter WHERE subgroup_member_id IN (SELECT id FROM SubgroupMember WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            Subgroups = DataBaseConnection.SQLiteConnection.Query<Subgroup>("SELECT * FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(s =>
            {
                s.FightSystem = FightSystems.Find(fs => fs.Id == s.FightSystemId);
                s.Members = new ObservableCollection<Member>(subgroupMembers.FindAll(sm => sm.SubgroupId == s.Id).ConvertAll(sm => sm.Member));
                s.Phases = new ObservableCollection<Phase>(phases.FindAll(p => p.SubgroupId == s.Id).ConvertAll(p =>
                {
                    p.ExtraMemberSubgroup = p.ExtraSubgroupMemberId != 0 ? subgroupMembers.First(sm => sm.Id == p.ExtraSubgroupMemberId) : null;
                    p.BattleProtocols = new ObservableCollection<BattleProtocol>(battleProtocols.FindAll(bp => bp.PhaseId == p.Id));
                    p.BattleProtocols.ToList().ForEach(bp => bp.Fighters = fighters.FindAll(f => f.BattleProtocolId == bp.Id).ConvertAll(f =>
                    {
                        Fighter opponent = fighters.First(opp => opp.Id != f.Id && opp.BattleProtocolId == f.BattleProtocolId);
                        f.SubgroupMember = subgroupMembers.First(sm => sm.Id == f.SubgroupMemberId);
                        f.Result = opponent.TotalScore == 0 && f.TotalScore == 0 ? FighterResultType.Nope : f.TotalScore > opponent.TotalScore ? FighterResultType.Winner : FighterResultType.Defeat;
                        return f;
                    }));
                    if (p.ExtraMemberSubgroup != null)
                        CreateBattleProtocolForLoneFighter(p, p.ExtraMemberSubgroup);
                    return p;
                }));
                return s;
            });

            Subgroups.FindAll(sub => sub.Name != "Финал").ForEach(s =>
            {
                Subgroup finalSubgroup = Subgroups.First(sub => s.GroupId == sub.GroupId && sub.Name == "Финал");
                s.HasFinalists = HasSubgroupFinalists(s.Members.ToList(), finalSubgroup.Members.ToList());
            });
            
            Groups = DataBaseConnection.SQLiteConnection.Query<Group>("SELECT * FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(g =>
            {
                g.Category = categories.First(c => tournamentCategories.Exists(tc => tc.Id == g.TournamentCategoryId && tc.CategoryId == c.Id));
                g.Nomination = nominations.First(n => tournamentNominations.Exists(tn => tn.Id == g.TournamentNominationId && tn.NominationId == n.Id));
                g.GroupRules = groupRules.FindAll(gr => groupRulesPerGroups.Exists(grpg => grpg.GroupId == g.Id && grpg.GroupRuleId == gr.Id));
                g.Subgroups = new ObservableCollection<Subgroup>(Subgroups.FindAll(s => s.GroupId == g.Id));
                List<Subgroup> notFinalSubgoups = GetNotFinalSubgroups(g);
                Subgroup finalSubgroup = g.Subgroups.First(s => s.Name == "Финал");
                g.IsFinalEnabled = finalSubgroup.Members.Count > 1 && notFinalSubgoups.All(s => s.HasFinalists);
                g.IsFinalCreated = finalSubgroup.Phases.Count > 0;
                g.IsFightsOver = notFinalSubgoups.All(s => IsAllProtocolsComplete(s));
                if (!g.IsFinalCreated)
                    g.Subgroups.Remove(finalSubgroup);
                return g;
            });
        }

        void UpdateData()
        {
            DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE subgroup_id=?", SelectedSubgroup.Id).ForEach(usm =>
            {
                SubgroupMember subgroupMember = subgroupMembers.First(sm => sm.Id == usm.Id);
                subgroupMember.IsDisqualified = usm.IsDisqualified;
                subgroupMember.Warnings = usm.Warnings;
            });

            if (IsAllProtocolsComplete(SelectedSubgroup))
            {
                Subgroup finalSubgroup = Subgroups.First(s => s.GroupId == SelectedGroup.Id && s.Name == "Финал");
                List<SubgroupMember> subgroupMembers = DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE subgroup_id=?", finalSubgroup.Id);
                List<SubgroupMember> deleteSubgroupMembers = this.subgroupMembers.FindAll(sm => sm.SubgroupId == finalSubgroup.Id && !subgroupMembers.Exists(s => s.Id == sm.Id));
                deleteSubgroupMembers.ForEach(dsm =>
                {
                    finalSubgroup.Members.Remove(finalSubgroup.Members.First(m => m.Id == dsm.MemberId));
                    this.subgroupMembers.RemoveAll(s => s.Id == dsm.Id);
                });
                subgroupMembers.FindAll(sm => !this.subgroupMembers.Exists(s => sm.Id == s.Id)).ForEach(sm =>
                {
                    sm.Member = SelectedSubgroup.Members.First(m => m.Id == sm.MemberId);
                    this.subgroupMembers.Add(sm);
                    finalSubgroup.Members.Add(sm.Member);
                });
                List<Subgroup> notFinalSubgroups = GetNotFinalSubgroups(SelectedGroup);
                notFinalSubgroups.ForEach(subgroup => subgroup.HasFinalists = HasSubgroupFinalists(subgroup.Members.ToList(), finalSubgroup.Members.ToList()));
                SelectedGroup.IsFinalEnabled = finalSubgroup.Members.Count > 1 && notFinalSubgroups.All(s => s.HasFinalists);
            }
        }

        void FinishTournament()
        {
            MainViewModel.Tournament.FightsCompleted = true;
            DataBaseConnection.UpdateTournament(MainViewModel.Tournament);
        }

        bool IsTournamentFinished() => !MainViewModel.Tournament.FightsCompleted && Groups.All(g => g.Subgroups.Any(s => s.Name == "Финал" && IsAllProtocolsComplete(s)));

        void UpdateNumber()
        {
            List<SubgroupMember> finalist = subgroupMembers.FindAll(sm => sm.SubgroupId == SelectedSubgroup.Id);
            for (int i = 0; i < finalist.Count; i++)
            {
                finalist[i].Number = i + 1;
                DataBaseConnection.SQLiteConnection.Update(finalist[i]);
            }
        }

        List<Subgroup> GetNotFinalSubgroups(Group forGroup) => forGroup.Subgroups.ToList().FindAll(s => s.Name != "Финал");
    }
}