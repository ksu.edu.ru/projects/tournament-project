using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Windows.Controls;
using System.Windows.Input;
using TournamentSoftware.Commands;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.Windows.BattleProtocol.Pages;

namespace TournamentSoftware.ViewModels
{
    class BattleProtocolViewModel : NotifyPropertyChangedObject
    {
        Page selectedPage;
        RoundTablePage roundTablePage;
        PreliminaryResultPage preliminaryResultPage;

        BattleProtocol battleProtocol;
        Round selectedRound;

        int countJudgeNotes;
        SaveStatus status;

        public BattleProtocolViewModel()
        {
            SelectedJudges = new ObservableCollection<ValueContainer<TournamentJudge>>();
            LoadData(MainViewModel.SelectedBattleProtocolId);
            SwitchPage(BattleProtocolPageType.RoundsData);

            AddJudgeNoteCommand = new LambdaCommand(OnAddJudgeCommandNoteExecuted, CanAddJudgeNoteCommandExecute);
            RemoveJudgeNoteCommand = new LambdaCommand(OnRemoveJudgeNoteCommandExecuted, CanRemoveJudgeNoteCommandExecute);
            AddRoundCommand = new LambdaCommand(OnAddRoundCommandExecuted, CanAddRoundCommandExecute);
            RemoveRoundCommand = new LambdaCommand(OnRemoveRoundCommandExecuted, CanRemoveRoundCommandExecute);
            OpenNextRoundCommand = new LambdaCommand(OnOpenNextRoundCommandExecuted, CanOpenNextRoundCommandExecute);
            OpenPrevRoundCommand = new LambdaCommand(OnOpenPrevRoundCommandExecuted, CanOpenPrevRoundCommandExecute);
            OpenPreliminaryResultCommand = new LambdaCommand(OnOpenPreliminaryResultCommandExecuted, CanOpenPreliminaryResultCommandExecute);
            OpenRoundsDataCommand = new LambdaCommand(OnOpenRoundsDataCommandExecuted, CanOpenRoundsDataCommandExecute);
            EndTheFightCommand = new LambdaCommand(OnEndTheFightCommandExecuted, CanEndTheFightCommandExecute);
        }

        #region Properties

        public Page SelectedPage
        {
            get => selectedPage;
            set => Set(ref selectedPage, value);
        }

        public BattleProtocol BattleProtocol
        {
            get => battleProtocol;
            private set => battleProtocol = value;
        }

        public ObservableCollection<Round> Rounds { get; set; }

        public Round SelectedRound
        {
            get => selectedRound;
            set => Set(ref selectedRound, value);
        }

        public List<TournamentJudge> TournamentJudges { get; set; }

        public ObservableCollection<ValueContainer<TournamentJudge>> SelectedJudges { get; set; }

        public Nomination Nomination { get; private set; }

        public Category Category { get; private set; }

        public SaveStatus Status
        {
            get => status;
            set => Set(ref status, value);
        }

        #endregion

        #region Commands

        #region AddJudgeNoteCommand

        public ICommand AddJudgeNoteCommand { get; set; }

        bool CanAddJudgeNoteCommandExecute(object parameter) => status == SaveStatus.New && TournamentJudges.Count > SelectedJudges.Count;

        void OnAddJudgeCommandNoteExecuted(object parameter) => AddJudgeNote();

        #endregion

        #region RemoveJudgeNoteCommand

        public ICommand RemoveJudgeNoteCommand { get; set; }

        bool CanRemoveJudgeNoteCommandExecute(object parameter) => status == SaveStatus.New && SelectedJudges.Count > 1;

        void OnRemoveJudgeNoteCommandExecuted(object parameter) => RemoveJudgeNote();

        #endregion

        #region RemoveRoundCommand

        public ICommand RemoveRoundCommand { get; set; }

        bool CanRemoveRoundCommandExecute(object parameter) => status == SaveStatus.New && Rounds.Count > 1;

        void OnRemoveRoundCommandExecuted(object parameter) => RemoveRound();

        #endregion

        #region AddRoundCommand

        public ICommand AddRoundCommand { get; set; }

        bool CanAddRoundCommandExecute(object parameter) => status == SaveStatus.New;

        void OnAddRoundCommandExecuted(object parameter) => AddRound();

        #endregion

        #region OpenPrevRoundCommand

        public ICommand OpenPrevRoundCommand { get; set; }

        bool CanOpenPrevRoundCommandExecute(object parameter) => Rounds.IndexOf(SelectedRound) > 0;

        void OnOpenPrevRoundCommandExecuted(object parameter) => SetSelectedRound(RoundSwitchType.Prev);

        #endregion

        #region OpenNextRoundCommand

        public ICommand OpenNextRoundCommand { get; set; }

        bool CanOpenNextRoundCommandExecute(object parameter) => Rounds.IndexOf(SelectedRound) + 1 < Rounds.Count;

        void OnOpenNextRoundCommandExecuted(object parameter) => SetSelectedRound(RoundSwitchType.Next);

        #endregion

        #region OpenPreliminaryResultCommand

        public ICommand OpenPreliminaryResultCommand { get; set; }

        bool CanOpenPreliminaryResultCommandExecute(object parameter) => SelectedJudges.All(judge => judge.Value != null) && SelectedJudges.GroupBy(sj => sj.Value.Id).All(g => g.Count() == 1);

        void OnOpenPreliminaryResultCommandExecuted(object parameter) => SwitchPage(BattleProtocolPageType.TotalData);

        #endregion

        #region OpenRoundsDataCommand

        public ICommand OpenRoundsDataCommand { get; set; }

        bool CanOpenRoundsDataCommandExecute(object parameter) => true;

        void OnOpenRoundsDataCommandExecuted(object parameter) => SwitchPage(BattleProtocolPageType.RoundsData);

        #endregion

        #region EndTheFightCommand
        
        public ICommand EndTheFightCommand { get; set; }

        bool CanEndTheFightCommandExecute(object parameter) => Status == SaveStatus.New && BattleProtocol.Fighters.Max(f => f.TotalScore) != BattleProtocol.Fighters.Min(f => f.TotalScore);
        
        void OnEndTheFightCommandExecuted(object parameter)
        {
            SaveData();
            Status = SaveStatus.Saved;
        }

        #endregion

        #endregion

        void SwitchPage(BattleProtocolPageType battleProtocolPageType)
        {
            switch (battleProtocolPageType)
            {
                case BattleProtocolPageType.RoundsData:
                    if (roundTablePage == null)
                        roundTablePage = new RoundTablePage();
                    SelectedPage = roundTablePage;
                    break;
                case BattleProtocolPageType.TotalData:
                    if (preliminaryResultPage == null)
                        preliminaryResultPage = new PreliminaryResultPage();
                    SelectedPage = preliminaryResultPage;
                    break;
            }
        }

        void LoadData(int battleProtocolId)
        {
            List<Member> members = DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT member_id FROM SubgroupMember WHERE id IN (SELECT subgroup_member_id FROM Fighter WHERE battle_protocol_id=?));", battleProtocolId);
            List<SubgroupMember> subgroupMembers = DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE id IN (SELECT subgroup_member_id FROM Fighter WHERE battle_protocol_id=?);", battleProtocolId).ConvertAll(sm =>
            {
                sm.Member = members.First(m => m.Id == sm.MemberId);
                return sm;
            });
            TournamentJudges = DataBaseConnection.SQLiteConnection.Query<TournamentJudge>("SELECT * FROM TournamentJudge WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            List<Judge> judges = DataBaseConnection.SQLiteConnection.Query<Judge>("SELECT * FROM Judge WHERE id IN (SELECT judge_id FROM TournamentJudge WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            TournamentJudges.ForEach(tj => tj.Judge = judges.First(j => j.Id == tj.JudgeId));
            List<RoundFighter> roundFighters = DataBaseConnection.SQLiteConnection.Query<RoundFighter>("SELECT * FROM RoundFighter WHERE round_id IN (SELECT id FROM Round WHERE battle_protocol_id=?);", battleProtocolId);
            List<JudgeNote> judgeNotes = DataBaseConnection.SQLiteConnection.Query<JudgeNote>("SELECT * FROM JudgeNote WHERE round_fighter_id IN (SELECT id FROM RoundFighter WHERE fighter_id IN (SELECT id FROM Fighter WHERE battle_protocol_id=?));", battleProtocolId);
            Nomination = DataBaseConnection.SQLiteConnection.Query<Nomination>("SELECT * FROM Nomination WHERE id=(SELECT nomination_id FROM TournamentNomination WHERE id=(SELECT tournament_nomination_id FROM [GROUP] WHERE id=(SELECT group_id FROM Subgroup WHERE id=(SELECT subgroup_id FROM Phase WHERE id=(SELECT phase_id FROM BattleProtocol WHERE id=?)))));", battleProtocolId).First();
            Category = DataBaseConnection.SQLiteConnection.Query<Category>("SELECT * FROM Category WHERE id=(SELECT category_id FROM TournamentCategory WHERE id=(SELECT tournament_category_id FROM [GROUP] WHERE id=(SELECT group_id FROM Subgroup WHERE id=(SELECT subgroup_id FROM Phase WHERE id=(SELECT phase_id FROM BattleProtocol WHERE id=?)))));", battleProtocolId).First();

            BattleProtocol = DataBaseConnection.SQLiteConnection.Get<BattleProtocol>(bp => bp.Id == battleProtocolId);
            BattleProtocol.Fighters = DataBaseConnection.SQLiteConnection.Query<Fighter>("SELECT * FROM Fighter WHERE battle_protocol_id=?;", battleProtocolId).ConvertAll(f =>
            {
                f.SubgroupMember = subgroupMembers.First(sm => sm.Id == f.SubgroupMemberId);
                return f;
            });

            Rounds = new ObservableCollection<Round>(DataBaseConnection.SQLiteConnection.Query<Round>("SELECT * FROM Round WHERE battle_protocol_id=?;", BattleProtocol.Id).ConvertAll(round => {
                round.RoundFighters = BattleProtocol.Fighters.ConvertAll(fighter =>
                {
                    RoundFighter roundFighter = roundFighters.Find(rf => rf.RoundId == round.Id && rf.FighterId == fighter.Id);
                    List<JudgeNote> roundFighterJudgeNotes = judgeNotes.FindAll(jnm => jnm.RoundFighterId == roundFighter.Id);
                    roundFighterJudgeNotes.Sort((x, y) => x.Number.CompareTo(y.Number));
                    countJudgeNotes = roundFighterJudgeNotes.Count;
                    roundFighter.Fighter = fighter;
                    roundFighter.JudgeNotes = new ObservableCollection<JudgeNote>(roundFighterJudgeNotes);
                    return roundFighter;
                });
                return round;
            }));

            countJudgeNotes = Rounds.FirstOrDefault()?.RoundFighters?.FirstOrDefault()?.JudgeNotes?.Count ?? 1;
            Status = Rounds.Count > 0 ? SaveStatus.Reopen : SaveStatus.New;

            if (Status == SaveStatus.Reopen)
                for (int number = 1; number <= countJudgeNotes; number++)
                    SelectedJudges.Add(new ValueContainer<TournamentJudge>(TournamentJudges.First(tj => tj.Id == judgeNotes.First(jn => jn.Number == number).TournamentJudgeId)));
            else
            {
                AddRound();
                for (int i = 1; i <= countJudgeNotes; i++)
                    SelectedJudges.Add(new ValueContainer<TournamentJudge>(null));
            }
            SelectedRound = Rounds[0];
        }

        void AddRound()
        {
            List<RoundFighter> roundFighters = new List<RoundFighter>();
            BattleProtocol.Fighters.ForEach(fighter =>
            {
                RoundFighter roundFighter = new RoundFighter { JudgeNotes = new ObservableCollection<JudgeNote>(), Fighter = fighter };
                for (int i = 1; i <= countJudgeNotes; i++)
                    roundFighter.JudgeNotes.Add(new JudgeNote { Number = countJudgeNotes });
                roundFighters.Add(roundFighter);
            });
            Rounds.Add(new Round { BattleProtocolId = battleProtocol.Id, RoundFighters = roundFighters, Number = Rounds.Count + 1 });
        }

        void RemoveRound()
        {
            int indexOfSelectedRound = Rounds.IndexOf(SelectedRound);
            SelectedRound = Rounds[indexOfSelectedRound == 0 ? indexOfSelectedRound + 1 : indexOfSelectedRound - 1];
            Rounds.RemoveAt(indexOfSelectedRound);
            for (int i = indexOfSelectedRound; i < Rounds.Count; i++)
                Rounds[i].Number -= 1;
        }

        void AddJudgeNote()
        {
            countJudgeNotes = countJudgeNotes + 1;
            SelectedJudges.Add(new ValueContainer<TournamentJudge>(null));
            Rounds.ToList().ForEach(round => round.RoundFighters.ForEach(roundFighter => roundFighter.JudgeNotes.Add(new JudgeNote { Number = countJudgeNotes })));
        }

        void RemoveJudgeNote()
        {
            countJudgeNotes = countJudgeNotes - 1;
            SelectedJudges.RemoveAt(SelectedJudges.Count - 1);
            Rounds.ToList().ForEach(round => round.RoundFighters.ForEach(roundFighter => roundFighter.JudgeNotes.RemoveAt(countJudgeNotes)));
        }

        void FindTheDifference(int index)
        {
            JudgeNote firstJudgeNote = SelectedRound.RoundFighters[0].JudgeNotes[index];
            JudgeNote secondJudgeNote = SelectedRound.RoundFighters[1].JudgeNotes[index];
            firstJudgeNote.Difference = firstJudgeNote.Point - secondJudgeNote.Point;
            secondJudgeNote.Difference = secondJudgeNote.Point - firstJudgeNote.Point;
        }

        void SaveData()
        {
            DataBaseConnection.SQLiteConnection.Update(BattleProtocol);
            DataBaseConnection.SQLiteConnection.UpdateAll(BattleProtocol.Fighters);
            Rounds.ToList().ForEach(round =>
            {
                DataBaseConnection.SQLiteConnection.Insert(round);
                round.RoundFighters.ForEach(roundFighter =>
                {
                    roundFighter.RoundId = round.Id;
                    DataBaseConnection.SQLiteConnection.Insert(roundFighter);
                    roundFighter.JudgeNotes.ToList().ForEach(judgeNote =>
                    {
                        judgeNote.TournamentJudgeId = SelectedJudges[judgeNote.Number - 1].Value.Id;
                        judgeNote.RoundFighterId = roundFighter.Id;
                        DataBaseConnection.SQLiteConnection.Insert(judgeNote);
                    });
                });
            });
        }

        void SetSelectedRound(RoundSwitchType roundSwitchType)
        {
            int indexOfSelectedRound = Rounds.IndexOf(SelectedRound);
            switch (roundSwitchType)
            {
                case RoundSwitchType.Prev:
                    if (indexOfSelectedRound > 0)
                        SelectedRound = Rounds[indexOfSelectedRound - 1];
                    break;
                case RoundSwitchType.Next:
                    if (indexOfSelectedRound < Rounds.Count - 1)
                        SelectedRound = Rounds[indexOfSelectedRound + 1];
                    break;
            }
        }
    }
}
