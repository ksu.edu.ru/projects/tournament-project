﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels.Base;

namespace TournamentSoftware.ViewModels
{
    class NominationViewModel : DataGridViewModel<Nomination>
    {
        List<TournamentNomination> tournamentNominations;

        public NominationViewModel() : base()
        {
            LoadData();
            CheckValid();

            Status = Tournament.RegistrationCompleted ? SaveStatus.Reopen : SaveStatus.New;
        }

        #region Command

        #region SaveCommand

        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            if (IsDuplicated)
            {
                MessageBox.Show("Названия должны быть уникальными.", "Ошибка", MessageBoxButton.OK);
                return;
            }
            AcceptChanges();
            Status = SaveStatus.Saved;
        }

        #endregion

        #endregion

        protected override void LoadData()
        {
            tournamentNominations = DataBaseConnection.SQLiteConnection.Query<TournamentNomination>("SELECT * FROM TournamentNomination;");
            DataBaseConnection.SQLiteConnection.Query<Nomination>("SELECT * FROM Nomination;").ForEach(nomination =>
            {
                nomination.SelectedForCurrentTournament = tournamentNominations.Exists(tc => tc.NominationId == nomination.Id && tc.TournamentId == MainViewModel.Tournament.Id);
                Items.Add(new EditableModel<Nomination>(nomination) { IsUnremovable = tournamentNominations.Exists(tournamentCategory => tournamentCategory.NominationId == nomination.Id && tournamentCategory.TournamentId != MainViewModel.Tournament.Id) });
            });
        }

        protected override bool IsItemDuplicate(Nomination first, Nomination second) => first.EqualProp(second, "Name");

        protected override Nomination CreateItem() => new Nomination();

        public override void AcceptChanges()
        {
            RemovedItems.ConvertAll(n => n.Backup).ForEach(nomination =>
            {
                if (tournamentNominations.Exists(tn => tn.NominationId == nomination.Id && tn.TournamentId == MainViewModel.Tournament.Id))
                    DeleteTournamentNomination(nomination);
                DataBaseConnection.SQLiteConnection.Delete<Nomination>(nomination.Id);
            });
            RemovedItems.Clear();

            Items.ToList().ForEach(nominationObjCh =>

            {
                if (nominationObjCh.IsNew)
                {
                    DataBaseConnection.SQLiteConnection.Insert(nominationObjCh.Data);
                    if (nominationObjCh.Data.SelectedForCurrentTournament)
                        CreateTournamentNomination(nominationObjCh.Data);
                } else if (nominationObjCh.IsChanged)
                {
                    if (nominationObjCh.CheckIsPropertyChanged("Name"))
                        DataBaseConnection.SQLiteConnection.Update(nominationObjCh.Data);
                    if (nominationObjCh.CheckIsPropertyChanged("SelectedForCurrentTournament"))
                        if (nominationObjCh.Data.SelectedForCurrentTournament)
                            CreateTournamentNomination(nominationObjCh.Data);
                        else
                            DeleteTournamentNomination(nominationObjCh.Data);
                }
                nominationObjCh.AcceptChanges();
            });

            IsChanged = false;
        }

        void CreateTournamentNomination(Nomination nomination)
        {
            TournamentNomination tournamentNomination = new TournamentNomination { NominationId = nomination.Id, TournamentId = MainViewModel.Tournament.Id };
            DataBaseConnection.SQLiteConnection.Insert(tournamentNomination);
            CreateGroups(tournamentNomination);
            tournamentNominations.Add(tournamentNomination);
        }

        void DeleteTournamentNomination(Nomination nomination)
        {
            TournamentNomination tournamentNomination = tournamentNominations.First(tn => tn.NominationId == nomination.Id && tn.TournamentId == MainViewModel.Tournament.Id);
            DeleteGroups(tournamentNomination.Id);
            DataBaseConnection.SQLiteConnection.Delete<TournamentNomination>(tournamentNomination.Id);
            tournamentNominations.Remove(tournamentNomination);
        }

        void CreateGroups(TournamentNomination tournamentNomination) => DataBaseConnection.SQLiteConnection.Query<TournamentCategory>("SELECT * FROM TournamentCategory WHERE tournament_id=?;", MainViewModel.Tournament.Id)
                .ForEach(tc => DataBaseConnection.SQLiteConnection.Insert( new Group { TournamentCategoryId = tc.Id, TournamentNominationId = tournamentNomination.Id }));

        void DeleteGroups(int tournamentNominationId) => DataBaseConnection.SQLiteConnection.Query<Group>("DELETE FROM [Group] WHERE tournament_nomination_id=?;", tournamentNominationId);
    }
}