﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using System.Windows.Input;
using TournamentSoftware.Commands;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using Group = TournamentSoftware.Models.Group;

namespace TournamentSoftware.ViewModels
{
    class SubgroupViewModel : NotifyPropertyChangedObject
    {
        List<EditableModel<Subgroup>> subgroups;
        EditableModel<Group> selectedGroup;
        List<SubgroupMember> subgroupMembers;
        int? countOfSubgroups;

        readonly Club emptyClub;
        readonly City emptyCity;

        public SubgroupViewModel()
        {
            subgroups = new List<EditableModel<Subgroup>>();
            SelectedSubgroups = new ObservableCollection<EditableModel<Subgroup>>();
            SelectedSubgroups.CollectionChanged += SelectedSubgroups_SubscribesRegulation_CollectionChanged;
            SelectedGroupRules = new ObservableCollection<GroupRule>();
            SelectedGroupRules.CollectionChanged += SelectedGroupRules_CollectionChanged; ;
            emptyCity = new City { Name = "Без города" };
            emptyClub = new Club { Name = "Без клуба", City = emptyCity };
            LoadData();

            GoToTournamentGridCommand = new LambdaCommand(OnGoToTournamentGridCommandExecuted, CanGoToTournamentGridCommandExecute);
            CreateSubgroupsCommand = new LambdaCommand(OnCreateSubgroupsCommandExecuted, CanCreateSubgroupsCommandExecute);
            MoveMemberToSubgroupCommand = new LambdaCommand(OnMoveMemberToSubgroupCommandExecuted, CanMoveMemberToSubgroupCommandExecute);
            SaveSubgroupsCommand = new LambdaCommand(OnSaveSubgroupsCommandExecuted, CanSaveSubgroupsCommandExecute);
            GoToRegistrateMembersCommand = new LambdaCommand(OnGoToRegistrateMembersCommandExecuted, CanGoToRegistrateMembersCommandExecute);
        }

        #region Properties

        public Tournament Tournament => MainViewModel.Tournament;

        public List<EditableModel<Group>> Groups { get; private set; }

        public List<GroupRule> GroupRules { get; private set; }

        bool SelectedGroupIncludeLeaderRule => SelectedGroupRules.Any(gr => gr.Name == "Правило посевных бойцов");

        bool SelectedGroupIncludeClubRule => SelectedGroupRules.Any(gr => gr.Name == "Правило одноклубников");

        bool SelectedGroupIncludeCityRule => SelectedGroupRules.Any(gr => gr.Name == "Правило города");

        public EditableModel<Group> SelectedGroup
        {
            get => selectedGroup;
            set
            {
                if (Set(ref selectedGroup, value))
                {
                    SelectedSubgroups.CollectionChanged -= SelectedSubgroups_ItemsRegulation_CollectionChanged;
                    SelectedSubgroups.ToList().ForEach(s => SelectedSubgroups.Remove(s));

                    SelectedGroupRules.CollectionChanged -= SelectedGroupRules_CollectionChanged;
                    SelectedGroupRules.ToList().ForEach(gr => SelectedGroupRules.Remove(gr));
                    if (selectedGroup != null)
                    {
                        subgroups.ToList().FindAll(s => selectedGroup.Data.Subgroups.Contains(s.Data)).ForEach(s => SelectedSubgroups.Add(s));
                        SelectedSubgroups.CollectionChanged += SelectedSubgroups_ItemsRegulation_CollectionChanged;

                        selectedGroup.Data.GroupRules.ForEach(gr => SelectedGroupRules.Add(gr));
                        SelectedGroupRules.CollectionChanged += SelectedGroupRules_CollectionChanged;
                    }
                    
                    CountOfSubgroups = selectedGroup != null ? SelectedSubgroups.Count : (int?)null;
                }
            }
        }

        public ObservableCollection<EditableModel<Subgroup>> SelectedSubgroups { get; private set; }

        public ObservableCollection<GroupRule> SelectedGroupRules { get; private set; }

        public int? CountOfSubgroups
        {
            get => countOfSubgroups;
            set => Set(ref countOfSubgroups, value);
        }

        #endregion

        #region Commands

        #region GoToRegistrateMembers

        public ICommand GoToRegistrateMembersCommand { get; }

        bool CanGoToRegistrateMembersCommandExecute(object parameter) => true;

        void OnGoToRegistrateMembersCommandExecuted(object parameter) 
        {
            bool isChanged = Groups.Exists(group => group.IsChanged && group.Data.Subgroups.Count > 0);
            if (isChanged && MessageBox.Show("Внесенные вами данные не сохранятся", "Вы действительно хотите выйти?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                return;
            MainViewModel.SwitchPage(MainPageType.MemberRegistration);
        }

        #endregion

        #region CreateSubgroupsCommand

        public ICommand CreateSubgroupsCommand { get; }

        bool CanCreateSubgroupsCommandExecute(object parameter) => SelectedGroup != null && CountOfSubgroups > 0 && SelectedGroup.Data.Members.Count / CountOfSubgroups >= 2;

        void OnCreateSubgroupsCommandExecuted(object parameter) => FormateSubgroups();

        #endregion

        #region MoveMemberToSubgroup

        public ICommand MoveMemberToSubgroupCommand { get; }

        bool CanMoveMemberToSubgroupCommandExecute(object parameter)
        {
            if (parameter == null)
                return false;
            object[] parameters = parameter as object[];
            Member selectMember = parameters[0] as Member;
            Subgroup subgroupToMove = parameters[1] as Subgroup;
            if (selectMember == null || subgroupToMove == null)
                return false;
            Subgroup subgroupFromMove = SelectedSubgroups.First(s => s.Data.Members.Contains(selectMember)).Data;
            return subgroupFromMove != subgroupToMove && subgroupFromMove.Members.Count > 2 && SelectedSubgroups.Count > 1;
        }

        void OnMoveMemberToSubgroupCommandExecuted(object parameter)
        {
            object[] parameters = parameter as object[];
            Member selectMember = parameters[0] as Member;
            Subgroup subgroupToMove = parameters[1] as Subgroup;
            MoveMember(subgroupToMove, selectMember);
        }

        #endregion

        #region SaveSubgroups

        public ICommand SaveSubgroupsCommand { get; }

        bool CanSaveSubgroupsCommandExecute(object parameter) => SelectedSubgroups.Count > 0 && selectedGroup.IsChanged;

        void OnSaveSubgroupsCommandExecuted(object parameter) => SaveGroupData();

        #endregion

        #region GoToTournamentGridCommand

        public ICommand GoToTournamentGridCommand { get; }

        bool CanGoToTournamentGridCommandExecute(object parameter) => Groups.All(group => !group.IsChanged);

        void OnGoToTournamentGridCommandExecuted(object parameter)
        {
            if (!Tournament.SubgroupsFormationCompleted)
            {
                Groups.ForEach(group => DataBaseConnection.SQLiteConnection.Insert(new Subgroup { Name = "Финал", FightSystemId = 2, GroupId = group.Data.Id }));
                Tournament.SubgroupsFormationCompleted = true;
                DataBaseConnection.UpdateTournament(Tournament);
            }
            MainViewModel.SwitchPage(MainPageType.TournamentGrid);
        }

        #endregion

        #endregion

        void LoadData()
        {
            List<Club> clubs = new List<Club> { emptyClub };
            List<City> cities = new List<City> { emptyCity };
            cities.AddRange(DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City WHERE id IN (SELECT DISTINCT city_id FROM Member WHERE id IN (SELECT member_id FROM GroupMember WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id));
            clubs.AddRange(DataBaseConnection.SQLiteConnection.Query<Club>("SELECT * FROM Club WHERE id IN (SELECT DISTINCT club_id FROM Member WHERE id IN (SELECT member_id FROM GroupMember WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?))));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(c => 
            {
                c.City = cities.First(city => city.Id == c.CityId);
                return c;
            }));

            GroupRules = DataBaseConnection.SQLiteConnection.Query<GroupRule>("SELECT * FROM GroupRule;");
            List<TournamentCategory> tournamentCategories = DataBaseConnection.SQLiteConnection.Query<TournamentCategory>("SELECT * FROM TournamentCategory WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            List<TournamentNomination> tournamentNominations = DataBaseConnection.SQLiteConnection.Query<TournamentNomination>("SELECT * FROM TournamentNomination WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            List<Category> categories = DataBaseConnection.SQLiteConnection.Query<Category>("SELECT * FROM Category WHERE id IN (SELECT category_id FROM TournamentCategory WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            List<Nomination> nominations = DataBaseConnection.SQLiteConnection.Query<Nomination>("SELECT * FROM Nomination WHERE id IN (SELECT nomination_id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            List<GroupMember> groupMembers = DataBaseConnection.SQLiteConnection.Query<GroupMember>("SELECT * FROM GroupMember WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            List<Member> members = DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT DISTINCT member_id FROM GroupMember WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(m =>
            {
                m.Club = clubs.Find(c => c.Id == m.ClubId);
                m.City = cities.Find(c => c.Id == m.CityId);
                return m;
            });
            List<GroupRule_Group> groupRulePerGroup = DataBaseConnection.SQLiteConnection.Query<GroupRule_Group>("SELECT * FROM GroupRule_Group WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            subgroupMembers = DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE subgroup_id IN (SELECT id FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
            subgroups.AddRange(DataBaseConnection.SQLiteConnection.Query<Subgroup>("SELECT * FROM Subgroup WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)) AND name<>\"Финал\";", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(s =>
            {
                s.Members = new ObservableCollection<Member>(subgroupMembers.FindAll(sm => sm.SubgroupId == s.Id).ConvertAll(sm => members.First(m => m.Id == sm.MemberId)));
                return new EditableModel<Subgroup>(s);
            }));
            Groups = DataBaseConnection.SQLiteConnection.Query<Group>("SELECT * FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ConvertAll(group =>
            {
                group.Category = categories.First(c => tournamentCategories.Exists(tc => tc.Id == group.TournamentCategoryId && tc.CategoryId == c.Id));
                group.Nomination = nominations.First(n => tournamentNominations.Exists(tn => tn.Id == group.TournamentNominationId && tn.NominationId == n.Id));
                group.GroupRules = groupRulePerGroup.FindAll(grpg => grpg.GroupId == group.Id).ConvertAll(grpg => GroupRules.First(gr => gr.Id == grpg.GroupRuleId));
                group.Members = groupMembers.FindAll(gm => gm.GroupId == group.Id).ConvertAll(gm => members.First(m => m.Id == gm.MemberId));
                group.Subgroups = new ObservableCollection<Subgroup>(subgroups.FindAll(s => s.Data.GroupId == group.Id).ConvertAll(s => s.Data));
                return new EditableModel<Group>(group, group.Subgroups.Count == 0) { ChangedOutSide = CheckGroupChangedStatus };
            });
        }

        bool CheckGroupChangedStatus(Group group) => subgroups.FindAll(s => s.Data.GroupId == group.Id).Exists(s => s.IsChanged);

        void SelectedSubgroups_SubscribesRegulation_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditableModel<Subgroup> item in e.NewItems)
                    {
                        item.PropertyChanged += Subgroup_PropertyChanged;
                        item.Data.Members.CollectionChanged += Members_CollectionChanged;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (EditableModel<Subgroup> item in e.OldItems)
                    {
                        item.PropertyChanged -= Subgroup_PropertyChanged;
                        item.Data.Members.CollectionChanged -= Members_CollectionChanged;
                    }
                    break;
            }
        }

        void Subgroup_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsChanged":
                    selectedGroup.CheckIsChanged();
                    break;
            }
        }

        void Members_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Remove:
                    SelectedSubgroups.First(s => s.Data.Members.Equals(sender)).CheckIsChanged();
                    break;
            }
        }

        void SelectedSubgroups_ItemsRegulation_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditableModel<Subgroup> item in e.NewItems)
                    {
                        selectedGroup.Data.Subgroups.Add(item.Data);
                        subgroups.Add(item);
                    }
                    selectedGroup.CheckIsChanged();
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (EditableModel<Subgroup> item in e.OldItems)
                    {
                        selectedGroup.Data.Subgroups.Remove(item.Data);
                        subgroups.Remove(item);
                    }
                    selectedGroup.CheckIsChanged();
                    break;
            }
        }

        void SelectedGroupRules_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (GroupRule item in e.NewItems)
                        selectedGroup.Data.GroupRules.Add(item);
                    selectedGroup.CheckIsChanged();
                    selectedGroup.Data.Subgroups.ToList().ForEach(s => s.IsRulesFailed = CheckIsRulesFailed(s));
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (GroupRule item in e.OldItems)
                        selectedGroup.Data.GroupRules.Remove(item);
                    selectedGroup.CheckIsChanged();
                    selectedGroup.Data.Subgroups.ToList().ForEach(s => s.IsRulesFailed = CheckIsRulesFailed(s));
                    break;
            }
        }

        void FormateSubgroups()
        {
            SelectedSubgroups.ToList().ForEach(s => SelectedSubgroups.Remove(s));

            List<Subgroup> newSubgroups = new List<Subgroup>();
            for (int i = 1; i <= CountOfSubgroups; i++)
                newSubgroups.Add(new Subgroup { Name = $"Подгруппа {i}", Members = new ObservableCollection<Member>(), GroupId = SelectedGroup.Data.Id });
            FillSubgroups(newSubgroups);
            newSubgroups.ForEach(s => SelectedSubgroups.Add(new EditableModel<Subgroup>(s, true)));
        }

        void FillSubgroups(List<Subgroup> subgroups)
        {
            int minMembersInSubgroup = selectedGroup.Data.Members.Count / (int)countOfSubgroups;
            List<Member> members = selectedGroup.Data.Members.ToList();

            AccordingToRulesFillSubgroup(subgroups, members, minMembersInSubgroup, true);

            for (int i = 0; i < 2; i++)
            {
                subgroups.FindAll(s => s.Members.Count < minMembersInSubgroup).ForEach(subgroup =>
                {
                    foreach (Subgroup fullSubgroup in subgroups.FindAll(s => s.Members.Count == minMembersInSubgroup))
                    {
                        Member member = AccordingToRulesFindMember(members, fullSubgroup);
                        if (member != null)
                        {
                            Member memberFromFullSubgroup = AccordingToRulesFindMember(fullSubgroup.Members.ToList(), subgroup);
                            if (memberFromFullSubgroup != null)
                            {
                                fullSubgroup.Members[fullSubgroup.Members.IndexOf(memberFromFullSubgroup)] = member;
                                subgroup.Members.Add(memberFromFullSubgroup);
                                members.Remove(member);
                            }
                        }
                    }
                });
                if (i < 1 && selectedGroup.Data.Members.Count % CountOfSubgroups > 0)
                    AccordingToRulesFillSubgroup(subgroups, members, minMembersInSubgroup);
                else
                    i++;
                minMembersInSubgroup = subgroups.Max(sub => sub.Members.Count);
            }

            members.ToList().ForEach(member =>
            {
                int maxMembersCount = subgroups.Max(s => s.Members.Count);
                int minMembersCount = subgroups.Min(s => s.Members.Count);
                foreach (Subgroup subgroup in subgroups)
                {
                    if (maxMembersCount == minMembersCount || (maxMembersCount > subgroup.Members.Count && minMembersCount == subgroup.Members.Count))
                    {
                        subgroup.Members.Add(member);
                        subgroup.IsRulesFailed = true;
                        members.Remove(member);
                        break;
                    }
                }
            });
        }

        void AccordingToRulesFillSubgroup(List<Subgroup> subgroups, List<Member> members, int membersInSubgroupCount, bool maxFill = false)
        {
            subgroups.ForEach(subgroup =>
            {
                while (0 < members.Count)
                {
                    if (maxFill && subgroup.Members.Count == membersInSubgroupCount)
                        break;
                    Member member = AccordingToRulesFindMember(members, subgroup);
                    if (member == null)
                        break;
                    subgroup.Members.Add(member);
                    members.Remove(member);
                    if (!maxFill)
                        break;
                }
            });
        }

        Member AccordingToRulesFindMember(List<Member> members, Subgroup subgroup) => members.Find(member =>
            (!SelectedGroupIncludeCityRule || member.City.Id == 0 || !subgroup.Members.Any(sm => member.City.Id == sm.City.Id))
            && (!SelectedGroupIncludeClubRule || member.Club.Id == 0 || !subgroup.Members.Any(m => member.Club.Id == m.Club.Id))
            && (!SelectedGroupIncludeLeaderRule || !member.Leader || !subgroup.Members.Any(m => m.Leader)));

        bool CheckIsRulesFailed(Subgroup subgroup) => 
            (SelectedGroupIncludeCityRule && !subgroup.Members.All(member => member.City.Id == 0 || subgroup.Members.First(m => m.City.Id == member.City.Id).Id == member.Id))
            || (SelectedGroupIncludeClubRule && !subgroup.Members.All(member => member.Club.Id == 0 || subgroup.Members.First(m => m.Club.Id == member.Club.Id).Id == member.Id))
            || (SelectedGroupIncludeLeaderRule && subgroup.Members.ToList().FindAll(m => m.Leader).Count > 1);

        void MoveMember(Subgroup subgroupToMove, Member member)
        {
            Subgroup subgroupFromMove = SelectedSubgroups.First(s => s.Data.Members.Contains(member)).Data;
            subgroupFromMove.Members.Remove(member);
            subgroupFromMove.IsRulesFailed = CheckIsRulesFailed(subgroupFromMove);

            subgroupToMove.Members.Add(member);
            subgroupToMove.IsRulesFailed = CheckIsRulesFailed(subgroupToMove);
        }

        void SaveGroupData()
        {
            if (selectedGroup.CheckIsPropertyChanged("GroupRules"))
            {
                selectedGroup.Backup?.GroupRules.FindAll(gr => !selectedGroup.Data.GroupRules.Contains(gr)).ForEach(gr => DataBaseConnection.SQLiteConnection.Delete<GroupRule_Group>(gr.Id));
                selectedGroup.Data.GroupRules.FindAll(gr => !selectedGroup.Backup?.GroupRules.Contains(gr) ?? false).ForEach(gr => DataBaseConnection.SQLiteConnection.Insert(new GroupRule_Group { GroupId = selectedGroup.Data.Id, GroupRuleId = gr.Id }));
            }

            selectedGroup.Backup?.Subgroups.ToList().FindAll(s => !selectedGroup.Data.Subgroups.Contains(s)).ForEach(s =>
            {
                DataBaseConnection.SQLiteConnection.Execute("DELETE FROM SubgroupMember WHERE subgroup_id=?;", s.Id);
                subgroupMembers.RemoveAll(sm => sm.SubgroupId == s.Id);
                DataBaseConnection.SQLiteConnection.Delete<Subgroup>(s.Id);
            });

            SelectedSubgroups.ToList().FindAll(s => s.IsChanged).ForEach(subgroup =>
            {
                if (subgroup.IsNew)
                {
                    DataBaseConnection.SQLiteConnection.Insert(subgroup.Data);
                    subgroup.Data.Members.ToList().ForEach(m =>
                    {
                        SubgroupMember subgroupMember = new SubgroupMember { SubgroupId = subgroup.Data.Id, MemberId = m.Id, Number = subgroup.Data.Members.IndexOf(m) + 1 };
                        DataBaseConnection.SQLiteConnection.Insert(subgroupMember);
                        subgroupMembers.Add(subgroupMember);
                    });
                } else
                {
                    if (subgroup.CheckIsPropertyChanged("Name"))
                        DataBaseConnection.SQLiteConnection.Update(subgroup.Data);
                    if (subgroup.Backup.Members.Count != subgroup.Data.Members.Count || !subgroup.Backup.Members.Any(member => subgroup.Data.Members.Any(m => m.Equals(member))))
                    {
                        subgroup.Data.Members.ToList().ForEach(member =>
                        {
                            SubgroupMember subgroupMember = subgroupMembers.Find(sm => sm.SubgroupId == subgroup.Data.Id && member.Id == sm.MemberId);
                            if (subgroupMember != null)
                            {
                                subgroupMember.Number = subgroup.Data.Members.IndexOf(member) + 1;
                                DataBaseConnection.SQLiteConnection.Update(subgroupMember);
                            }
                        });

                        subgroup.Backup.Members.ToList()
                            .FindAll(m => !subgroup.Data.Members.Contains(m))
                            .ConvertAll(m => subgroupMembers.First(sm => sm.MemberId == m.Id && sm.SubgroupId == subgroup.Backup.Id))
                            .ForEach(sm =>
                            {
                                DataBaseConnection.SQLiteConnection.Delete<SubgroupMember>(sm.Id);
                                subgroupMembers.Remove(sm);
                            });

                        subgroup.Data.Members.ToList()
                            .FindAll(m => !subgroup.Backup.Members.Contains(m))
                            .ForEach(m =>
                            {
                                SubgroupMember subgroupMember = new SubgroupMember { MemberId = m.Id, SubgroupId = subgroup.Data.Id };
                                DataBaseConnection.SQLiteConnection.Insert(subgroupMember);
                                subgroupMembers.Add(subgroupMember);
                            });
                    }
                }
                subgroup.AcceptChanges();
            });

            SelectedGroup.AcceptChanges();
        }
    }
}