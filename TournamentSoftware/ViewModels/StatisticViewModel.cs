using System;
using System.Collections.Generic;
using System.Linq;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.ViewModels.Base;

namespace TournamentSoftware.ViewModels
{
    class StatisticViewModel : DataGridViewModel<SubgroupMember>
    {
        Subgroup subgroup;
        Subgroup finalSubgroup;
        List<SubgroupMember> subgroupMembersFinalSubgroup;

        bool isSubgroupFinished;

        public StatisticViewModel() : base()
        {
            LoadData();
            CheckValid();

            Status = Tournament.FightsCompleted ? Types.SaveStatus.Reopen : Types.SaveStatus.New;
        }

        public bool CanEditFinalists
        {
            get => isSubgroupFinished;
            set => Set(ref isSubgroupFinished, value);
        }

        #region Commands

        #region SaveDataCommand

        protected override bool CanSaveDataCommandExecute(object parameter) => IsValid && IsChanged && Status != Types.SaveStatus.Reopen && Items.Any(stm => !stm.Data.IsDisqualified);

        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            AcceptChanges();
            Status = Types.SaveStatus.Saved;
        }

        #endregion

        #endregion

        protected override void LoadData()
        {
            subgroup = DataBaseConnection.SQLiteConnection.Get<Subgroup>(s => s.Id == MainViewModel.SelectedSubgroupId);
            List<SubgroupMember> subgroupMembers = DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE subgroup_id=?;", subgroup.Id);
            List<Member> members = DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT member_id FROM SubgroupMember WHERE subgroup_id=?);", subgroup.Id);
            List<BattleProtocol> battleProtocols = DataBaseConnection.SQLiteConnection.Query<BattleProtocol>("SELECT * FROM BattleProtocol WHERE phase_id IN (SELECT id FROM Phase WHERE subgroup_id=?) AND (SELECT count(*) FROM Round WHERE Round.battle_protocol_id=BattleProtocol.id)>0;", subgroup.Id);
            List<Fighter> fighters = DataBaseConnection.SQLiteConnection.Query<Fighter>("SELECT * FROM Fighter WHERE battle_protocol_id IN (SELECT id FROM BattleProtocol WHERE phase_id IN (SELECT id FROM Phase WHERE subgroup_id=?) AND (SELECT count(*) FROM Round WHERE Round.battle_protocol_id=BattleProtocol.id)>0);", subgroup.Id);
            List<RoundFighter> roundFighters = DataBaseConnection.SQLiteConnection.Query<RoundFighter>("SELECT * FROM RoundFighter WHERE fighter_id IN (SELECT id FROM Fighter WHERE subgroup_member_id IN (SELECT id FROM SubgroupMember WHERE subgroup_id=?));", subgroup.Id);
            List<JudgeNote> judgeNotes = DataBaseConnection.SQLiteConnection.Query<JudgeNote>("SELECT * FROM JudgeNote WHERE round_fighter_id IN (SELECT id FROM RoundFighter WHERE fighter_id IN (SELECT id FROM Fighter WHERE subgroup_member_id IN (SELECT id FROM SubgroupMember WHERE subgroup_id=?)));", subgroup.Id);
            if (subgroup.Name == "Финал")
            {
                finalSubgroup = subgroup;
                subgroupMembersFinalSubgroup = subgroupMembers;
            } else
            {
                finalSubgroup = DataBaseConnection.SQLiteConnection.Get<Subgroup>(s => s.Name == "Финал" && s.GroupId == subgroup.GroupId);
                subgroupMembersFinalSubgroup = DataBaseConnection.SQLiteConnection.Query<SubgroupMember>("SELECT * FROM SubgroupMember WHERE subgroup_id=?", finalSubgroup.Id);
            }
            subgroupMembers.ForEach(sm =>
            {
                sm.Member = members.First(m => m.Id == sm.MemberId);
                sm.IsSelectedForFinal = subgroupMembersFinalSubgroup.Exists(smf => smf.MemberId == sm.MemberId);
                fighters.FindAll(f => f.SubgroupMemberId == sm.Id).ForEach(f =>
                {
                    Fighter opponent = fighters.First(fr => fr.BattleProtocolId == f.BattleProtocolId && fr.SubgroupMemberId != f.SubgroupMemberId);
                    List<RoundFighter> rounds = roundFighters.FindAll(rf => rf.FighterId == f.Id);
                    if (opponent.TotalScore < f.TotalScore)
                        sm.WinCount++;
                    else
                        sm.DefeatCount++;
                    judgeNotes.FindAll(jn => rounds.Exists(rf => rf.Id == jn.RoundFighterId)).ForEach(jn => sm.CountOfWarnings += jn.CountOfWarnings); ;
                    sm.TotalScore = sm.TotalScore + f.TotalScore;
                });
                Items.Add(new EditableModel<SubgroupMember>(sm));
            });
            UpdateRating();

            CanEditFinalists = finalSubgroup.Id != subgroup.Id && DataBaseConnection.SQLiteConnection.Query<Phase>("SELECT * FROM Phase WHERE subgroup_id=?;", finalSubgroup.Id).Count == 0 && battleProtocols.Count > 0 && DataBaseConnection.SQLiteConnection.Query<BattleProtocol>("SELECT * FROM BattleProtocol WHERE phase_id IN (SELECT id FROM Phase WHERE subgroup_id=?);", MainViewModel.SelectedSubgroupId).Count == battleProtocols.Count;
        }

        void UpdateRating()
        {
            int index = 0;
            Items.OrderBy(stmchobj => stmchobj.Data.IsDisqualified).ThenByDescending(stmchobj => stmchobj.Data.WinCount).ThenByDescending(stmchobj => stmchobj.Data.TotalScore).ThenByDescending(stmchobj => stmchobj.Data.CountOfWarnings).ToList().ForEach(m =>
            {
                m.Data.Rating = m.Data.IsDisqualified ? 0 : index + 1;
                Items.Move(Items.IndexOf(m), index);
                index++;
            });
        }

        public override void AcceptChanges()
        {
            bool needToUpdate = false;
            Items.ToList().FindAll(smco => smco.IsChanged).ForEach(sm =>
            {
                if (sm.CheckIsPropertyChanged("Warnings") || sm.CheckIsPropertyChanged("IsDisqualified"))
                {
                    needToUpdate = needToUpdate || sm.CheckIsPropertyChanged("IsDisqualified");
                    DataBaseConnection.SQLiteConnection.Update(sm.Data);
                }
                if (CanEditFinalists && sm.CheckIsPropertyChanged("IsSelectedForFinal"))
                    if (sm.Data.IsSelectedForFinal)
                    {
                        SubgroupMember newFinalist = new SubgroupMember { SubgroupId = finalSubgroup.Id, MemberId = sm.Data.MemberId };
                        DataBaseConnection.SQLiteConnection.Insert(newFinalist);
                        subgroupMembersFinalSubgroup.Add(newFinalist);
                    } else
                    {
                        SubgroupMember deletedFinalist = subgroupMembersFinalSubgroup.First(stm => stm.MemberId == sm.Data.MemberId);
                        DataBaseConnection.SQLiteConnection.Delete<SubgroupMember>(deletedFinalist.Id);
                        subgroupMembersFinalSubgroup.Remove(deletedFinalist);
                    }
                sm.AcceptChanges();
            });

            if (needToUpdate)
                UpdateRating();
        }

        protected override SubgroupMember CreateItem() => throw new NotImplementedException();

        protected override bool IsItemDuplicate(SubgroupMember first, SubgroupMember second) => false;
    }
}
