﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Windows.Input;
using TournamentSoftware.Commands;
using TournamentSoftware.Models;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;
using TournamentSoftware.Types;
using TournamentSoftware.Infrastructure.Events.EventArgs;

namespace TournamentSoftware.ViewModels.Base
{
    abstract class DataGridViewModel<TModel> : NotifyPropertyChangedObject, IChangeTracking, IValidObject, IValidatorObject, IGetterSaveStatus, IDublicatedObject where TModel : INotifyPropertyChanged, IDataErrorInfo, ICloneable, IEquatableProps<TModel>, IPreviewPropertyChanged
    {
        bool isChanged;
        bool isAllSelected;
        bool isValid;
        bool isDublicated;
        SaveStatus status;

        public DataGridViewModel()
        {
            SelectedItems = new ObservableCollection<EditableModel<TModel>>();
            Items = new ObservableCollection<EditableModel<TModel>>();
            Items.CollectionChanged += OnItemsCollectionChanged;
            RemovedItems = new List<EditableModel<TModel>>();
            Items.CollectionChanged += IsAllSelectedCheck;
            SelectedItems.CollectionChanged += SelectedCollectionChanged;
            SelectedItems.CollectionChanged += IsAllSelectedCheck;

            ChangeAllSelectionCommand = new LambdaCommand(OnChangeAllSelectionCommandExecuted, CanChangeAllSelectionCommandExecute);
            ChangeItemSelectionCommand = new LambdaCommand(OnChangeItemSelectionCommandExecuted, CanChangeItemSelectionCommandExecute);
            AddItemCommand = new LambdaCommand(OnAddItemCommandExecuted, CanAddItemCommandExecute);
            SaveDataCommand = new LambdaCommand(OnSaveDataCommandExecuted, CanSaveDataCommandExecute);
            RemoveItemsCommand = new LambdaCommand(OnRemoveItemsCommandExecuted, CanRemoveItemsCommandExecute);
        }

        public Tournament Tournament => MainViewModel.Tournament;

        public ObservableCollection<EditableModel<TModel>> Items { get; set; }

        public ObservableCollection<EditableModel<TModel>> SelectedItems { get; set; }

        protected List<EditableModel<TModel>> RemovedItems { get; set; }

        public bool IsChanged
        {
            get => isChanged;
            set => Set(ref isChanged, value);
        }

        public bool IsAllSelected
        {
            get => isAllSelected;
            set => Set(ref isAllSelected, value);
        }

        public bool IsValid
        {
            get => isValid;
            set => Set(ref isValid, value);
        }

        public bool IsDuplicated
        {
            get => isDublicated;
            set => Set(ref isDublicated, value);
        }

        public SaveStatus Status
        {
            get => status;
            set => Set(ref status, value);
        }

        #region Commands

        #region ChangeAllSelectionCommand

        public ICommand ChangeAllSelectionCommand { get; }

        bool CanChangeAllSelectionCommandExecute(object parameter) => Items.Count != 0;

        void OnChangeAllSelectionCommandExecuted(object parameter)
        {
            if (IsAllSelected)
                Items.ToList().FindAll(item => !item.IsSelected).ForEach(item => SelectedItems.Add(item));
            else
                SelectedItems.ToList().ForEach(selectedItem => SelectedItems.Remove(selectedItem));
        }

        #endregion

        #region ChangeItemSelectionCommand

        public ICommand ChangeItemSelectionCommand { get; }

        bool CanChangeItemSelectionCommandExecute(object parameter) => true;

        void OnChangeItemSelectionCommandExecuted(object parameter)
        {
            EditableModel<TModel> model = parameter as EditableModel<TModel>;
            if (model.IsSelected)
                SelectedItems.Add(model);
            else
                SelectedItems.Remove(model);
        }

        #endregion

        #region AddItemCommand

        public ICommand AddItemCommand { get; }

        bool CanAddItemCommandExecute(object parameter) => Status != SaveStatus.Reopen;

        void OnAddItemCommandExecuted(object parameter)
        {
            Items.Add(new EditableModel<TModel>(CreateItem(), true));
            IsValid = false;
            IsChanged = true;
        }

        #endregion

        #region RemoveItemsCommand

        public ICommand RemoveItemsCommand { get; }

        bool CanRemoveItemsCommandExecute(object parameter) => Status != SaveStatus.Reopen && SelectedItems.Count > 0;

        void OnRemoveItemsCommandExecuted(object parameter)
        {
            SelectedItems.ToList().ForEach(item =>
            {
                if (!item.IsNew)
                    RemovedItems.Add(item);
                Items.Remove(item);
            });
            SelectedItems.Clear();
            CheckValid();
            CheckIsItemsEdited();
            CheckAllDuplicate();
        }

        #endregion

        #region SaveDataCommandExecute

        public ICommand SaveDataCommand { get; }

        protected virtual bool CanSaveDataCommandExecute(object parameter) => IsValid && IsChanged && !IsDuplicated;

        protected virtual void OnSaveDataCommandExecuted(object parameter) => AcceptChanges();

        #endregion

        #endregion

        public bool CheckValid() => IsValid = Items.Count == 0 || Items.ToList().All(item => item.IsValid);

        protected void CheckIsItemsEdited() => IsChanged = RemovedItems.Count > 0 || Items.Any(item => item.IsChanged);

        protected void CheckAllDuplicate() => IsDuplicated = Items.Any(item => item.IsDuplicated);

        protected void CheckDuplicate(object sender, PropertyChangedEventArgs e)
        {
            int shift = e is PreviewPropertyChangedEventArgs ? 1 : 0;
            List<EditableModel<TModel>> models = Items.ToList().FindAll(i => IsItemDuplicate(i.Data, (TModel)sender));
            if (models.Count - shift > 1)
                models.ForEach(m => m.IsDuplicated = true);
            else
                models.ForEach(m => m.IsDuplicated = false);
        }

        protected abstract bool IsItemDuplicate(TModel first, TModel second);

        protected abstract void LoadData();

        protected abstract TModel CreateItem();

        public abstract void AcceptChanges();

        void IsAllSelectedCheck(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                case NotifyCollectionChangedAction.Reset:
                case NotifyCollectionChangedAction.Replace:
                    IsAllSelected = Items.Count != 0 && Items.All(item => item.IsSelected);
                    break;
                case NotifyCollectionChangedAction.Remove:
                    IsAllSelected = false;
                    break;
            }
        }

        void SelectedCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditableModel<TModel> item in e.NewItems)
                        item.IsSelected = true;
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (EditableModel<TModel> item in e.OldItems)
                        item.IsSelected = false;
                    break;
            }
        }

        void OnItemsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditableModel<TModel> item in e.NewItems)
                    {
                        item.PropertyChanged += CheckEditProperty;
                        item.Data.PreviewPropertyChanged += CheckDuplicate;
                        item.Data.PropertyChanged += CheckDuplicate;
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (EditableModel<TModel> item in e.OldItems)
                    {
                        item.PropertyChanged -= CheckEditProperty;
                        item.Data.PreviewPropertyChanged -= CheckDuplicate;
                        item.Data.PropertyChanged -= CheckDuplicate;
                    }
                    break;
            }
        }

        protected void CheckEditProperty(object sender, PropertyChangedEventArgs e)
        {
            switch (e.PropertyName)
            {
                case "IsChanged":
                    CheckIsItemsEdited();
                    break;
                case "IsValid":
                    CheckValid();
                    break;
                case "IsDuplicated":
                    CheckAllDuplicate();
                    break;
            }
        }
    }
}