using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.Commands;
using System.Windows.Input;

namespace TournamentSoftware.ViewModels
{
    class StartViewModel : NotifyPropertyChangedObject
    {
        Tournament selectedTournament;

        public StartViewModel()
        {
            LoadData();
            CreateTournamentCommand = new LambdaCommand(OnCreateTournamentCommandExecuted, CanCreateTournamentCommandExecute);
            GoToCreateTournamentCommand = new LambdaCommand(OnGoToCreateTournamentCommandExecuted, CanGoToCreateTournamentCommandExecute);
            OpenTournamentCommand = new LambdaCommand(OnOpenTournamentCommandExecuted, CanOpenTournamentCommandExecute);
        }

        #region Properties

        public ObservableCollection<Tournament> Tournaments { get; set; }

        public Tournament SelectedTournament
        {
            get => selectedTournament;
            set => Set(ref selectedTournament, value);
        }

        #endregion

        #region Commands

        #region GoToCreateTournamentCommand

        public ICommand GoToCreateTournamentCommand { get; }

        bool CanGoToCreateTournamentCommandExecute(object parameter) => SelectedTournament?.Id != 0;

        void OnGoToCreateTournamentCommandExecuted(object parameter)
        {
            SelectedTournament = null;
            SelectedTournament = new Tournament();
        }

        #endregion

        #region GoToRegistrationCommand

        public ICommand CreateTournamentCommand { get; }

        bool CanCreateTournamentCommandExecute(object parameter) => SelectedTournament != null && IsUnique(SelectedTournament);

        void OnCreateTournamentCommandExecuted(object parameter)
        {
            MainViewModel.Tournament = CreateTournament();
            SelectedTournament = null;
            MainViewModel.SwitchPage(MainPageType.MemberRegistration);
        }

        #endregion

        #region OpenTournamentCommand

        public ICommand OpenTournamentCommand { get; }

        bool CanOpenTournamentCommandExecute(object parameter) => SelectedTournament != null && SelectedTournament.Id != 0;

        void OnOpenTournamentCommandExecuted(object parameter)
        {
            MainViewModel.Tournament = SelectedTournament;
            SelectedTournament = null;
            MainViewModel.SwitchPage(MainPageType.MemberRegistration);
        }

        #endregion

        #endregion

        void LoadData()
        {
            List<Tournament> tournaments = DataBaseConnection.SQLiteConnection.Query<Tournament>("SELECT * FROM Tournament;");
            Tournaments = new ObservableCollection<Tournament>(tournaments.OrderByDescending(t => t.Date));
        }

        Tournament CreateTournament()
        {
            SelectedTournament.Date = DateTime.Now;
            DataBaseConnection.SQLiteConnection.Insert(SelectedTournament);
            Tournaments.Insert(0, SelectedTournament);
            return SelectedTournament;
        }

        bool IsUnique(Tournament newTournament) => !Tournaments.ToList().Exists(t => t.Name == newTournament.Name);
    }
}
