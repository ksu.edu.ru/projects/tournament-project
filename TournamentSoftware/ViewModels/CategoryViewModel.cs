﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Infrastructure.Events.EventArgs;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels.Base;

namespace TournamentSoftware.ViewModels
{
    class CategoryViewModel : DataGridViewModel<Category>
    {
        List<TournamentCategory> tournamentCategories;

        public CategoryViewModel() : base()
        {
            LoadData();
            CheckValid();
            Status = Tournament.RegistrationCompleted ? SaveStatus.Reopen : SaveStatus.New;
        }

        #region Command

        #region SaveCommand

        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            if (IsDuplicated)
            {
                MessageBox.Show("Названия должны быть уникальными.", "Ошибка", MessageBoxButton.OK);
                return;
            }
            AcceptChanges();
            Status = SaveStatus.Saved;
        }

        #endregion

        #endregion

        protected override void LoadData()
        {
            tournamentCategories = DataBaseConnection.SQLiteConnection.Query<TournamentCategory>("SELECT * FROM TournamentCategory;");
            DataBaseConnection.SQLiteConnection.Query<Category>("SELECT * FROM Category;").ForEach(category =>
            {
                category.SelectedForCurrentTournament = tournamentCategories.Exists(tc => tc.CategoryId == category.Id && tc.TournamentId == MainViewModel.Tournament.Id);
                Items.Add(new EditableModel<Category>(category) { IsUnremovable = tournamentCategories.Exists(tournamentCategory => tournamentCategory.CategoryId == category.Id && tournamentCategory.TournamentId != MainViewModel.Tournament.Id) });
            });
        }

        protected override bool IsItemDuplicate(Category first, Category second) => first.EqualProp(second, "Name");

        protected override Category CreateItem() => new Category();

        public override void AcceptChanges()
        {
            RemovedItems.ConvertAll(c => c.Backup).ForEach(category =>
            {
                if (tournamentCategories.Exists(tc => tc.CategoryId == category.Id && tc.TournamentId == MainViewModel.Tournament.Id))
                    DeleteTournamentCategory(category);
                DataBaseConnection.SQLiteConnection.Delete<Category>(category.Id);
            });
            RemovedItems.Clear();

            Items.ToList().ForEach(categoryObjCh =>
            {
                if (categoryObjCh.IsNew)
                {
                    DataBaseConnection.SQLiteConnection.Insert(categoryObjCh.Data);
                    if (categoryObjCh.Data.SelectedForCurrentTournament)
                        CreateTournamentCategory(categoryObjCh.Data);
                } else if (categoryObjCh.IsChanged)
                {
                    if (categoryObjCh.CheckIsPropertyChanged("Name"))
                        DataBaseConnection.SQLiteConnection.Update(categoryObjCh.Data);
                    if (categoryObjCh.CheckIsPropertyChanged("SelectedForCurrentTournament"))
                        if (categoryObjCh.Data.SelectedForCurrentTournament)
                            CreateTournamentCategory(categoryObjCh.Data);
                        else
                            DeleteTournamentCategory(categoryObjCh.Data);
                }
                categoryObjCh.AcceptChanges();
            });

            IsChanged = false;
        }

        void CreateTournamentCategory(Category category)
        {
            TournamentCategory tournamentCategory = new TournamentCategory { CategoryId = category.Id, TournamentId = MainViewModel.Tournament.Id };
            DataBaseConnection.SQLiteConnection.Insert(tournamentCategory);
            CreateGroups(tournamentCategory);
            tournamentCategories.Add(tournamentCategory);
        }

        void DeleteTournamentCategory(Category category)
        {
            TournamentCategory tournamentCategory = tournamentCategories.First(tc => tc.CategoryId == category.Id && tc.TournamentId == MainViewModel.Tournament.Id);
            DeleteGroups(tournamentCategory.Id);
            DataBaseConnection.SQLiteConnection.Delete<TournamentCategory>(tournamentCategory.Id);
            tournamentCategories.Remove(tournamentCategory);
        }

        void CreateGroups(TournamentCategory tournamentCategory) => DataBaseConnection.SQLiteConnection.Query<TournamentNomination>("SELECT * FROM TournamentNomination WHERE tournament_id=?;", MainViewModel.Tournament.Id)
            .ForEach(tn => DataBaseConnection.SQLiteConnection.Insert(new Group { TournamentCategoryId = tournamentCategory.Id, TournamentNominationId = tn.Id }));

        void DeleteGroups(int tournamentCategoryId) => DataBaseConnection.SQLiteConnection.Execute("DELETE FROM [Group] WHERE tournament_nomination_id=?;", tournamentCategoryId);
    }
}