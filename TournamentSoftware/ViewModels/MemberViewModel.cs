using ExcelDataReader;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Data;
using System.Windows.Input;
using TournamentSoftware.Commands;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels.Base;
using TournamentSoftware.Windows.Judge;
using TournamentSoftware.Windows.Settings;
using Group = TournamentSoftware.Models.Group;

namespace TournamentSoftware.ViewModels
{
    class MemberViewModel : DataGridViewModel<Member>
    {
        List<Category> categories;
        List<TournamentCategory> tournamentCategories;
        List<Nomination> nominations;
        List<TournamentNomination> tournamentNominations;
        List<TournamentJudge> tournamentJudges;
        readonly City emptyCity;
        readonly Club emptyClub;
        List<string> requiredColumnsHeaders = new List<string> { "Имя", "Фамилия", "Отчество", "Псевдоним", "Пол", "Рост", "Вес", "Рейтинг (общий)", "Рейтинг (клубный)", "Клуб", "Город", "Категория", "Год рождения" };

        public MemberViewModel() : base()
        {
            emptyCity = new City { Name = "Без города" };
            emptyClub = new Club { Name = "Без клуба", City = emptyCity };
            Items.CollectionChanged += Items_CollectionChanged;
            Status = Tournament.RegistrationCompleted ? SaveStatus.Reopen : SaveStatus.New;
            LoadData();
            CheckValid();
            LoadFromFileCommand = new LambdaCommand(OnLoadFromFileCommandExecuted, CanLoadFromFileCommandExecute);
            OpenSettingsCommand = new LambdaCommand(OnOpenSettingsCommandExecuted, CanOpenSettingsCommandExecute);
            OpenJudgesCommand = new LambdaCommand(OnOpenJudgesCommandExecuted, CanOpenJudgesCommandExecute);
            OpenTournaments = new LambdaCommand(OnOpenTournamentsExecuted, CanOpenTournamentsExecute);
        }

        public ObservableCollection<Group> Groups { get; set; }

        public ObservableCollection<Club> Clubs { get; set; }

        public ObservableCollection<City> Cities { get; set; }

        #region Command

        #region SaveCommand

        protected override bool CanSaveDataCommandExecute(object parameter) => IsValid && IsChanged && !IsDuplicated && Items.Count > 1 || Status == SaveStatus.Reopen;
        
        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            if (IsIncorrectNumberMembersInGroups())
            {
                MessageBox.Show("В группах должно быть больше 1 участника", "Ошибка", MessageBoxButton.OK);
                return;
            }

            if (tournamentJudges.Count == 0)
            {
                MessageBox.Show("Минимум один судья должен участвовать в турнире", "Ошибка!");
                return;
            }

            if (Status != SaveStatus.Reopen)
            {
                AcceptChanges();
                Status = SaveStatus.Reopen;
            }
            MainViewModel.SwitchPage(MainPageType.SubgroupsFormation);
        }

        #endregion

        #region LoadFromFileCommand

        public ICommand LoadFromFileCommand { get; set; }

        bool CanLoadFromFileCommandExecute(object parameter) => Status != SaveStatus.Reopen;

        void OnLoadFromFileCommandExecuted(object parameter)
        {
            if (Items.Count > 0)
            {
                MessageBoxResult messageBoxResult = MessageBox.Show("Все предыдущие записи в таблице регистрации будут удалены. Вы хотите продолжить?", "Предупреждение", MessageBoxButton.OKCancel, MessageBoxImage.Warning, MessageBoxResult.Cancel);
                if (MessageBoxResult.OK != messageBoxResult)
                    return;
            }
            OpenFileDialog openFileDialog = new OpenFileDialog { Filter = "EXCEL Files (*.xlsx)|*.xlsx|EXCEL Files 2003 (*.xls)|*.xls|All files (*.*)|*.*" };

            if (openFileDialog.ShowDialog() == false)
                return;

            FileStream stream = File.Open(openFileDialog.FileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            if (!Regex.IsMatch(openFileDialog.SafeFileName, ".xlsx?"))
            {
                MessageBox.Show("Не удалось прочитать таблицу! Попробуйте загрузить другой файл", "Ошибка");
                return;
            }
            IExcelDataReader reader = ExcelReaderFactory.CreateReader(stream);
            DataTable dataTable = reader.AsDataSet().Tables[0];

            reader.Close();
            stream.Close();

            DataColumnCollection loadedColumns = dataTable.Columns;
            DataRowCollection loadedRows = dataTable.Rows;
            if (loadedRows.Count > 0 && requiredColumnsHeaders.All(rch => loadedRows[0].ItemArray.ToList().Exists(ia => ia.ToString() == rch)))
                LoadDataFromFile(loadedColumns, loadedRows);
            else
                MessageBox.Show("Не удалось прочитать таблицу! Попробуйте загрузить другой файл", "Ошибка");
        }

        #endregion

        #region OpenSettingsCommand

        public ICommand OpenSettingsCommand { get; set; }

        bool CanOpenSettingsCommandExecute(object parametr) => true;

        void OnOpenSettingsCommandExecuted(object parametr)
        {
            SettingsWindow settingsWindow = new SettingsWindow();

            if ((bool)settingsWindow.ShowDialog())
            {
                UpdateData();
                CollectionViewSource.GetDefaultView(Clubs).Refresh();
                CollectionViewSource.GetDefaultView(Cities).Refresh();
            }
        }

        #endregion

        #region OpenJudgesCommand

        public ICommand OpenJudgesCommand { get; set; }

        bool CanOpenJudgesCommandExecute(object parametr) => true;

        void OnOpenJudgesCommandExecuted(object parametr)
        {
            JudgeWindow judgeWindow = new JudgeWindow();
            if ((bool)judgeWindow.ShowDialog())
                LoadTournamentJudges();
        }

        #endregion

        #region OpenTournaments

        public ICommand OpenTournaments { get; set; }

        bool CanOpenTournamentsExecute(object parametr) => true;

        void OnOpenTournamentsExecuted(object parametr) 
        {
            if (IsChanged && Status == SaveStatus.New && MessageBox.Show("Внесенные вами данные не сохранятся.", "Вы действительно хотите выйти?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                return;
            MainViewModel.SwitchPage(MainPageType.Start);

        } 

        #endregion

        #endregion

        protected override void LoadData()
        {
            Groups = new ObservableCollection<Group>();
            Clubs = new ObservableCollection<Club>() { emptyClub };
            Cities = new ObservableCollection<City>() { emptyCity };

            LoadGroups();
            LoadTournamentJudges();

            DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City;").ForEach(c => Cities.Add(c));
            DataBaseConnection.SQLiteConnection.Query<Club>("SELECT * FROM Club;").ForEach(c =>
            {
                c.City = Cities.First(city => city.Id == c.CityId);
                Clubs.Add(c);
            });

            if (Status == SaveStatus.Reopen)
            {
                List<GroupMember> groupMembers = DataBaseConnection.SQLiteConnection.Query<GroupMember>("SELECT * FROM GroupMember WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);
                DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT member_id FROM GroupMember WHERE group_id IN (SELECT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?)));", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ForEach(m =>
                {
                    m.Club = Clubs.First(c => c.Id == m.ClubId);
                    m.City = Cities.First(c => c.Id == m.CityId);
                    m.Groups = new ObservableCollection<Group>(groupMembers.FindAll(gm => m.Id == gm.MemberId).ConvertAll(gm => Groups.First(g => g.Id == gm.GroupId)));
                    Items.Add(new EditableModel<Member>(m));
                });
            }
        }

        protected override bool IsItemDuplicate(Member first, Member second) => first.EqualProp(second, "Name") && first.EqualProp(second, "Surname") && first.EqualProp(second, "YearOfBirth");

        protected override Member CreateItem() => new Member { Groups = new ObservableCollection<Group>(), City = emptyCity, Club = emptyClub };

        public override void AcceptChanges()
        {
            RemoveEmptyGroups();

            Items.ToList().ForEach(member =>
            {
                if (member.IsNew)
                    DataBaseConnection.SQLiteConnection.Insert(member.Data);
                else if (member.IsChanged) // Работает с группами FIXME
                    DataBaseConnection.SQLiteConnection.Update(member.Data);
                member.AcceptChanges();
                InsetrMemberInGroups(member.Data);
            });

            MainViewModel.Tournament.RegistrationCompleted = true;
            DataBaseConnection.UpdateTournament(MainViewModel.Tournament);
        }

        void LoadGroups()
        {
            categories = DataBaseConnection.SQLiteConnection.Query<Category>("SELECT * FROM Category WHERE id IN (SELECT category_id FROM TournamentCategory WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            tournamentCategories = DataBaseConnection.SQLiteConnection.Query<TournamentCategory>("SELECT * FROM TournamentCategory WHERE tournament_id=?;", MainViewModel.Tournament.Id);

            nominations = DataBaseConnection.SQLiteConnection.Query<Nomination>("SELECT * FROM Nomination WHERE id IN (SELECT nomination_id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            tournamentNominations = DataBaseConnection.SQLiteConnection.Query<TournamentNomination>("SELECT * FROM TournamentNomination WHERE tournament_id=?;", MainViewModel.Tournament.Id);

            DataBaseConnection.SQLiteConnection.Query<Group>("SELECT * FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id).ForEach(group =>
            {
                group.Category = categories.First(c => c.Id == tournamentCategories.First(tc => tc.Id == group.TournamentCategoryId).CategoryId);
                group.Nomination = nominations.First(n => n.Id == tournamentNominations.First(tn => tn.Id == group.TournamentNominationId).NominationId);
                Groups.Add(group);
            });
        }

        void LoadTournamentJudges() => tournamentJudges = DataBaseConnection.SQLiteConnection.Query<TournamentJudge>("SELECT * FROM TournamentJudge WHERE tournament_id=?;", MainViewModel.Tournament.Id);

        void LoadDataFromFile(DataColumnCollection loadedColumns, DataRowCollection loadedRows)
        {
            List<Member> members = new List<Member>();
            List<Category> categories = new List<Category>();
            List<Nomination> nominations = new List<Nomination>();
            List<City> cities = new List<City>();
            List<Club> clubs = new List<Club>();
            for (int i = 1; i < loadedRows.Count; i++)
            {
                DataRow row = loadedRows[i];
                Member member = new Member { Groups = new ObservableCollection<Group>() };
                Category memberCategory = null;
                string cityName = null;
                string clubName = null;
                List<Nomination> memberNominations = new List<Nomination>();
                for (int j = 0; j < loadedColumns.Count; j++)
                {
                    switch (loadedRows[0].ItemArray[j])
                    {
                        case "Имя":
                            string validName = Regex.Replace(row.ItemArray[j].ToString(), "[^А-Яа-яЁё]*[ ]*", "");
                            member.Name = validName.Length > 20 ? validName.Substring(0, 20) : validName;
                            break;
                        case "Фамилия":
                            string validSurname = Regex.Replace(row.ItemArray[j].ToString(), "[^А-Яа-яЁё]*[ ]*", "");
                            member.Surname = validSurname.Length > 20 ? validSurname.Substring(0, 20) : validSurname;
                            break;
                        case "Отчество":
                            string validPatronymic = Regex.Replace(row.ItemArray[j].ToString(), "[^А-Яа-яЁё]", "");
                            member.Patronymic = validPatronymic.Length > 35 ? validPatronymic.Substring(0, 35) : validPatronymic;
                            break;
                        case "Псевдоним":
                            string validAlias = row.ItemArray[j].ToString();
                            member.Alias = validAlias.Length > 35 ? validAlias.Substring(0, 35) : validAlias;
                            break;
                        case "Клуб":
                            string validClub = row.ItemArray[j].ToString();
                            clubName = string.IsNullOrWhiteSpace(validClub) ? null : validClub.Length > 35 ? validClub.Substring(0, 35) : validClub;
                            break;
                        case "Город":
                            string validCity = Regex.Replace(row.ItemArray[j].ToString(), "[^А-Яа-яЁё]", "");
                            cityName = string.IsNullOrWhiteSpace(validCity) ? null : validCity.Length > 35 ? validCity.Substring(0, 35) : validCity;
                            break;
                        case "Посевной":
                            member.Leader = Regex.IsMatch(row.ItemArray[j].ToString(), "True");
                            break;
                        case "Пол":
                            if (row.ItemArray[j].ToString().Equals("М") || row.ItemArray[j].ToString().Equals("Ж"))
                                member.Sex = row.ItemArray[j].ToString();
                            break;
                        case "Год рождения":
                            string validYearOfBirth = Regex.Replace(row.ItemArray[j].ToString(), "[^0-9]", "");
                            if (int.TryParse(validYearOfBirth, out _))
                            {
                                int year = int.Parse(validYearOfBirth);
                                if (year <= DateTime.Now.Year & year.ToString().Length == 4)
                                    member.YearOfBirth = year;
                            }
                            break;
                        case "Категория":
                            string categoryName = row.ItemArray[j].ToString().Trim(' ');
                            if (string.IsNullOrWhiteSpace(categoryName))
                                break;
                            memberCategory = categories.Find(category => category.Name == categoryName);
                            if (memberCategory == null)
                            {
                                memberCategory = new Category { Name = categoryName };
                                categories.Add(memberCategory);
                            }
                            break;
                        case "Рост":
                            string validHeight = Regex.Replace(row.ItemArray[j].ToString(), "[^0-9]", "");
                            if (int.TryParse(validHeight, out _))
                            {
                                int height = int.Parse(validHeight);
                                if (height >= 100)
                                    member.Height = height;
                            }
                            break;
                        case "Вес":
                            string validWeight = Regex.Replace(row.ItemArray[j].ToString(), "[^0-9]", "");
                            if (int.TryParse(validWeight, out _))
                            {
                                int weight = int.Parse(validWeight);
                                if (weight >= 10)
                                    member.Weight = weight;
                            }
                            break;
                        case "Рейтинг (общий)":
                            string validRating = Regex.Replace(row.ItemArray[j].ToString(), "[^0-9]", "");
                            if (int.TryParse(validRating, out _))
                            {
                                int raiting = int.Parse(row.ItemArray[j].ToString());
                                member.CommonRating = raiting;
                            }
                            break;
                        case "Рейтинг (клубный)":
                            validRating = Regex.Replace(row.ItemArray[j].ToString(), "[^0-9]", "");
                            if (int.TryParse(validRating, out _))
                            {
                                int raiting = int.Parse(validRating);
                                member.ClubRating = raiting;
                            }
                            break;
                        default:
                            if (row.ItemArray[j].ToString() == "True")
                            {
                                string validNominationName = Regex.Replace(loadedRows[0].ItemArray[j].ToString().Trim(' '), "[^A-Za-zА-Яа-яЁё ]", "");
                                if (string.IsNullOrWhiteSpace(validNominationName))
                                    break;
                                Nomination memberNomination = nominations.Find(nomination => nomination.Name == validNominationName);
                                if (memberNomination == null)
                                {
                                    memberNomination = new Nomination { Name = validNominationName };
                                    nominations.Add(memberNomination);
                                }
                                memberNominations.Add(memberNomination);
                            }
                            break;
                    }
                }

                City city = cityName != null ? cities.Find(c => c.Name == cityName) : emptyCity;

                if (city == null)
                {
                    city = new City { Name = cityName };
                    cities.Add(city);
                }
                member.City = city;

                Club club = clubName != null ? clubs.Find(c => c.Name == clubName && c.City == city) : emptyClub;

                if (club == null)
                {
                    club = new Club { Name = clubName, City = city };
                    clubs.Add(club);
                }
                member.Club = club;

                if (memberCategory != null)
                    memberNominations.ForEach(memberNomination => member.Groups.Add(new Group { Category = memberCategory, Nomination = memberNomination }));
                Member memberModel = members.Find(m => m.Equals(member));
                if (memberModel != null)
                    member.Groups.ToList().FindAll(g => memberModel.Groups.ToList().Exists(gr => gr.Category.Name != g.Category.Name || gr.Nomination.Name != g.Nomination.Name)).ForEach(g => memberModel.Groups.Add(g));
                else
                    members.Add(member);
            }
            LoadFromFile(categories, nominations, members, cities, clubs);
        }

        void LoadFromFile(List<Category> categories, List<Nomination> nominations, List<Member> members, List<City> cities, List<Club> clubs)
        {
            List<Member> savedMembers = DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member;", MainViewModel.Tournament.Id);

            Items.ToList().ForEach(chobj => chobj.PropertyChanged -= CheckEditProperty);
            Items.ToList().ForEach(i => Items.Remove(i));
            Groups.ToList().ForEach(g => DataBaseConnection.SQLiteConnection.Delete<Group>(g.Id));
            Groups.Clear();

            LoadCategoryFromFile(categories);
            LoadNominationFromFile(nominations);

            tournamentCategories.ForEach(tournamentCategory =>
                tournamentNominations.ForEach(tournamentNomination =>
                {
                    Group group = new Group
                    {
                        Category = this.categories.First(c => c.Id == tournamentCategory.CategoryId),
                        TournamentCategoryId = tournamentCategory.Id,
                        Nomination = this.nominations.First(n => n.Id == tournamentNomination.NominationId),
                        TournamentNominationId = tournamentNomination.Id
                    };
                    DataBaseConnection.SQLiteConnection.Insert(group);
                    Groups.Add(group);
                })
            );

            SaveCities(cities);
            SaveClubs(clubs);

            members.ForEach(member =>
            {
                member.City = Cities.First(c => c.Name == member.City.Name);
                member.Club = Clubs.First(c => c.Name == member.Club.Name && c.City.Name == member.City.Name);
                member.Groups = new ObservableCollection<Group>(member.Groups.ToList().ConvertAll(group => Groups.ToList().Find(g => g.Category.Name == group.Category.Name && g.Nomination.Name == group.Nomination.Name)));

                Member savedMember = savedMembers.Find(m => m.Name == member.Name && m.Surname == member.Surname && m.YearOfBirth == member.YearOfBirth);
                if (savedMember == null)
                    Items.Add(new EditableModel<Member>(member, true));
                else
                {
                    member.Id = savedMember.Id;
                    Items.Add(new EditableModel<Member>(member));
                }

            });
        }

        void LoadCategoryFromFile(List<Category> fileCategories)
        {
            DataBaseConnection.SQLiteConnection.Execute("DELETE FROM TournamentCategory WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            categories.Clear();
            tournamentCategories.Clear();

            fileCategories.ForEach(categoryModel =>
            {
                Category category = DataBaseConnection.SQLiteConnection.Find<Category>(c => c.Name == categoryModel.Name);
                if (category == null)
                {
                    category = new Category { Name = categoryModel.Name };
                    DataBaseConnection.SQLiteConnection.Insert(category);
                }
                categories.Add(category);

                TournamentCategory tournamentCategory = new TournamentCategory { CategoryId = category.Id, TournamentId = MainViewModel.Tournament.Id };
                DataBaseConnection.SQLiteConnection.Insert(tournamentCategory);
                tournamentCategories.Add(tournamentCategory);
            });
        }

        void LoadNominationFromFile(List<Nomination> fileNominations)
        {
            DataBaseConnection.SQLiteConnection.Execute("DELETE FROM TournamentNomination WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            nominations.Clear();
            tournamentNominations.Clear();

            fileNominations.ForEach(nominationModel =>
            {
                Nomination nomination = DataBaseConnection.SQLiteConnection.Find<Nomination>(n => n.Name == nominationModel.Name);
                if (nomination == null)
                {
                    nomination = new Nomination { Name = nominationModel.Name };
                    DataBaseConnection.SQLiteConnection.Insert(nomination);
                }
                nominations.Add(nomination);
                TournamentNomination tournamentNomination = new TournamentNomination { NominationId = nomination.Id, TournamentId = MainViewModel.Tournament.Id };
                DataBaseConnection.SQLiteConnection.Insert(tournamentNomination);
                tournamentNominations.Add(tournamentNomination);
            });
        }

        void SaveClubs(List<Club> fileClubs)
        {
            fileClubs.FindAll(c => !Clubs.ToList().Exists(dbc => dbc.Name == c.Name && dbc.City.Name == c.City.Name)).ForEach(club =>
            {
                club.City = Cities.First(c => c.Name == club.City.Name);
                club.CityId = club.City.Id;
                DataBaseConnection.SQLiteConnection.Insert(club);
                Clubs.Add(club);
            });
        }

        void SaveCities(List<City> fileCities)
        {
            fileCities.FindAll(c => !Cities.ToList().Exists(dbc => dbc.Name == c.Name)).ForEach(city =>
            {
                DataBaseConnection.SQLiteConnection.Insert(city);
                Cities.Add(city);
            });
        }

        void UpdateData()
        {
            UpdateGroups();
            UpdateCities();
            UpdateClubs();
        }

        void UpdateGroups()
        {
            categories = DataBaseConnection.SQLiteConnection.Query<Category>("SELECT * FROM Category WHERE id IN (SELECT category_id FROM TournamentCategory WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            nominations = DataBaseConnection.SQLiteConnection.Query<Nomination>("SELECT * FROM Nomination WHERE id IN (SELECT nomination_id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id);
            tournamentCategories = DataBaseConnection.SQLiteConnection.Query<TournamentCategory>("SELECT * FROM TournamentCategory WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            tournamentNominations = DataBaseConnection.SQLiteConnection.Query<TournamentNomination>("SELECT * FROM TournamentNomination WHERE tournament_id=?;", MainViewModel.Tournament.Id);
            List<Group> groups = DataBaseConnection.SQLiteConnection.Query<Group>("SELECT * FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id=?) AND tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id=?);", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id);

            Groups.ToList().FindAll(group => !groups.Exists(g => g.Id == group.Id)).ForEach(group =>
            {
                Items.ToList().ForEach(memberCheObj => memberCheObj.Data.Groups.Remove(group));
                Groups.Remove(group);
            });

            Groups.ToList().ForEach(group =>
            {
                group.Category = categories.First(c => c.Id == group.Category.Id);
                group.Nomination = nominations.First(n => n.Id == group.Nomination.Id);
            });

            groups.FindAll(group => !Groups.ToList().Exists(g => g.Id == group.Id)).ForEach(group =>
            {
                group.Nomination = nominations.First(n => n.Id == tournamentNominations.First(tn => tn.Id == group.TournamentNominationId).NominationId);
                group.Category = categories.First(c => c.Id == tournamentCategories.First(tc => tc.Id == group.TournamentCategoryId).CategoryId);
                Groups.Add(group);
            });
        }

        void UpdateClubs()
        {
            List<Club> clubs = DataBaseConnection.SQLiteConnection.Query<Club>("SELECT * FROM Club;").ConvertAll(club =>
            {
                club.City = Cities.First(c => club.CityId == c.Id);
                return club;
            });

            Clubs.ToList().FindAll(club => club.Id != 0 && !clubs.Exists(c => c.Id == club.Id)).ForEach(club =>
            {
                Items.ToList().ConvertAll(chobj => chobj.Data).FindAll(m => m?.Club.Id == club.Id).ForEach(m => m.Club = emptyClub);
                Clubs.Remove(club);
            });

            clubs.ForEach(club =>
            {
                Club clubToUpdate = Clubs.ToList().Find(c => c.Id == club.Id);
                if (clubToUpdate == null)
                    Clubs.Add(club);
                else
                {
                    clubToUpdate.Name = club.Name;
                    clubToUpdate.City = club.City;
                }
            });
        }

        void UpdateCities()
        {
            List<City> cities = DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City;");
            Cities.ToList().FindAll(city => city.Id != 0 && !cities.Exists(c => c.Id == city.Id)).ForEach(city =>
            {
                Items.ToList().ConvertAll(co => co.Data).FindAll(m => m?.City.Id == city.Id).ForEach(m => m.City = emptyCity);
                Cities.Remove(city);
            });
            cities.ForEach(city =>
            {
                City cityToUpdate = Cities.ToList().Find(c => c.Id == city.Id);
                if (cityToUpdate == null)
                    Cities.Add(city);
                else
                    cityToUpdate.Name = city.Name;
            });
        }

        void RemoveEmptyGroups()
        {
            Groups.ToList().FindAll(group => IsGroupEmpty(group)).ForEach(group =>
            {
                DataBaseConnection.SQLiteConnection.Delete<Group>(group.Id);
                Groups.Remove(group);
                TournamentCategory tournamentCategory = tournamentCategories.Find(tc => tc.CategoryId == group.Category.Id && !Groups.ToList().Exists(g => g.Category.Id == tc.CategoryId));
                if (tournamentCategory != null)
                {
                    DataBaseConnection.SQLiteConnection.Delete<TournamentCategory>(tournamentCategory.Id);
                    tournamentCategories.Remove(tournamentCategory);
                }
                TournamentNomination tournamentNomination = tournamentNominations.Find(tn => tn.NominationId == group.Nomination.Id && !Groups.ToList().Exists(g => g.Nomination.Id == tn.NominationId));
                if (tournamentNomination != null)
                {
                    DataBaseConnection.SQLiteConnection.Delete<TournamentNomination>(tournamentNomination.Id);
                    tournamentNominations.Remove(tournamentNomination);
                }
            });
        }

        bool IsIncorrectNumberMembersInGroups() => Groups.ToList().Exists(group => Items.ToList().FindAll(memberChObj => memberChObj.Data.Groups.Contains(group)).Count() == 1);

        bool IsGroupEmpty(Group group) => Items.ToList().FindAll(memberChObj => memberChObj.Data.Groups.Contains(group)).Count == 0;

        void InsetrMemberInGroups(Member member) => member.Groups.ToList().ForEach(g => DataBaseConnection.SQLiteConnection.Insert(new GroupMember() { GroupId = g.Id, MemberId = member.Id }));

        void Items_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (EditableModel<Member> item in e.NewItems)
                        item.Data.Groups.CollectionChanged += MemberGroupsCollectionChanged;
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (EditableModel<Member> item in e.OldItems)
                        item.Data.Groups.CollectionChanged -= MemberGroupsCollectionChanged;
                    break;
            }
        }

        void MemberGroupsCollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            EditableModel<Member> memberChObj = Items.First(item => item.Data.Groups.Equals(sender));
            memberChObj.Data.Groups = memberChObj.Data.Groups;
        }
    }
}
