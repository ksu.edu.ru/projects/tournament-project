using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels.Base;

namespace TournamentSoftware.ViewModels
{
    class JudgeViewModel : DataGridViewModel<Judge>
    {
        List<TournamentJudge> tournamentJudges;
        readonly Club emptyClub;
        readonly City emptyCity;

        public JudgeViewModel() : base()
        {
            emptyCity = new City { Name = "Без города" };
            emptyClub = new Club { Name = "Без клуба", City = emptyCity };
            LoadData();
            CheckValid();
            Status = Tournament.RegistrationCompleted ? SaveStatus.Reopen : SaveStatus.New;
        }

        #region Command

        #region SaveCommand

        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            AcceptChanges();
            Status = SaveStatus.Saved;
        }

        #endregion

        #endregion

        public ObservableCollection<Club> Clubs { get; set; }

        public ObservableCollection<City> Cities { get; set; }

        protected override bool IsItemDuplicate(Judge first, Judge second) => first.EqualProp(second, "Name") && first.EqualProp(second, "Surname") && first.EqualProp(second, "Patronymic");

        protected override void LoadData()
        {
            tournamentJudges = DataBaseConnection.SQLiteConnection.Query<TournamentJudge>("SELECT * FROM TournamentJudge WHERE tournament_id=?;", MainViewModel.Tournament.Id);

            Cities = new ObservableCollection<City> { emptyCity };
            Clubs = new ObservableCollection<Club> { emptyClub };
            DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City;").ForEach(city => Cities.Add(city));
            DataBaseConnection.SQLiteConnection.Query<Club>("SELECT * FROM Club;").ForEach(club =>
            {
                club.City = Cities.First(city => city.Id == club.CityId);
                Clubs.Add(club);
            });

            DataBaseConnection.SQLiteConnection.Query<Judge>("SELECT * FROM Judge;").ForEach(judge =>
            {
                judge.Club = Clubs.First(club => club.Id == judge.ClubId);
                judge.City = Cities.First(city => city.Id == judge.CityId);
                judge.SelectedForCurrentTournament = tournamentJudges.Exists(tj => tj.JudgeId == judge.Id);
                Items.Add(new EditableModel<Judge>(judge));
            });
        }

        protected override Judge CreateItem() => new Judge { Club = emptyClub, City = emptyCity };

        public override void AcceptChanges()
        {
            RemovedItems.ConvertAll(j => j.Backup).ForEach(judge =>
            {
                if (judge.SelectedForCurrentTournament)
                    DeleteTournamentJudge(judge);
                DataBaseConnection.SQLiteConnection.Delete<Judge>(judge.Id);
            });
            RemovedItems.Clear();

            Items.ToList().ForEach(judgeChObj =>
            {
                if (judgeChObj.IsNew)
                {
                    DataBaseConnection.SQLiteConnection.Insert(judgeChObj.Data);
                    if (judgeChObj.Data.SelectedForCurrentTournament)
                        CreateTournamentJudge(judgeChObj.Data);
                } else if (judgeChObj.IsChanged)
                {
                    if (judgeChObj.CheckIsPropertyChanged("Surname") || judgeChObj.CheckIsPropertyChanged("Name") || judgeChObj.CheckIsPropertyChanged("Patronymic") || judgeChObj.CheckIsPropertyChanged("Club") || judgeChObj.CheckIsPropertyChanged("City"))
                        DataBaseConnection.SQLiteConnection.Update(judgeChObj.Data);
                    if (judgeChObj.CheckIsPropertyChanged("SelectedForCurrentTournament"))
                        if (judgeChObj.Data.SelectedForCurrentTournament)
                            CreateTournamentJudge(judgeChObj.Data);
                        else
                            DeleteTournamentJudge(judgeChObj.Data);
                }
                judgeChObj.AcceptChanges();
            });

            IsChanged = false;
        }

        void CreateTournamentJudge(Judge judge)
        {
            TournamentJudge tournamentJudge = new TournamentJudge { JudgeId = judge.Id, TournamentId = MainViewModel.Tournament.Id };
            DataBaseConnection.SQLiteConnection.Insert(tournamentJudge);
            tournamentJudges.Add(tournamentJudge);
        }

        void DeleteTournamentJudge(Judge judge)
        {
            TournamentJudge tournamentJudge = tournamentJudges.First(tj => tj.JudgeId == judge.Id);
            DataBaseConnection.SQLiteConnection.Delete<TournamentJudge>(tournamentJudge.Id);
            tournamentJudges.Remove(tournamentJudge);
        }
    }
}