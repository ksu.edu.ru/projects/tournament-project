﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Windows;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.ViewModels.Base;

namespace TournamentSoftware.ViewModels
{
    class CityViewModel : DataGridViewModel<City>
    {
        public CityViewModel() : base()
        {
            LoadData();
            CheckValid();

            Status = Tournament.RegistrationCompleted ? Types.SaveStatus.Reopen : Types.SaveStatus.New;
        }

        #region Command

        #region SaveCommand

        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            if (IsDuplicated)
            {
                MessageBox.Show("Названия должны быть уникальными.", "Ошибка", MessageBoxButton.OK);
                return;
            }
            AcceptChanges();
            Status = Types.SaveStatus.Saved;
        }

        #endregion

        #endregion

        protected override bool IsItemDuplicate(City first, City second) => first.EqualProp(second, "Name");

        protected override void LoadData() => DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City;").ForEach(city => Items.Add(new EditableModel<City>(city) { IsUnremovable = IsUsedInTournaments(city) }));

        protected override City CreateItem() => new City();

        public override void AcceptChanges()
        {
            RemovedItems.ConvertAll(c => c.Backup).ForEach(city =>
            {
                DataBaseConnection.SQLiteConnection.Execute("UPDATE Judge SET city_id=0 WHERE city_id=?;", city.Id);
                DataBaseConnection.SQLiteConnection.Execute("UPDATE Club SET city_id=0 WHERE city_id=?;", city.Id);
                DataBaseConnection.SQLiteConnection.Delete<City>(city.Id);
            });
            RemovedItems.Clear();

            Items.ToList().ForEach(cityChObj =>
            {
                if (cityChObj.IsNew)
                    DataBaseConnection.SQLiteConnection.Insert(cityChObj.Data);
                else if (cityChObj.IsChanged)
                    DataBaseConnection.SQLiteConnection.Update(cityChObj.Data);
                cityChObj.AcceptChanges();
            });

            IsChanged = false;
        }

        bool IsUsedInTournaments(City item) => DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT member_id FROM GroupMember WHERE group_id IN (SELECT DISTINCT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id IN (SELECT id FROM Tournament WHERE id<>?)) OR tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id IN (SELECT id FROM Tournament WHERE id<>?)))) AND city_id=?;", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id, item.Id).Count != 0
                || DataBaseConnection.SQLiteConnection.Query<Judge>("SELECT * FROM Judge WHERE id IN (SELECT judge_id FROM TournamentJudge WHERE tournament_id IN (SELECT id FROM Tournament WHERE id<>?)) AND city_id=?;", MainViewModel.Tournament.Id, item.Id).Count != 0;
    }
}