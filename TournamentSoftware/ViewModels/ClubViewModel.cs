﻿using System.Collections.Generic;
using System.Linq;
using System.Windows;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels.Base;

namespace TournamentSoftware.ViewModels
{
    class ClubViewModel : DataGridViewModel<Club>
    {
        readonly City emptyCity = new City { Name = "Без города" };

        public ClubViewModel() : base()
        {
            LoadData();
            CheckValid();
            Status = Tournament.RegistrationCompleted ? SaveStatus.Reopen : SaveStatus.New;
        }

        public List<City> Cities { get; set; }

        #region Command

        #region SaveCommand

        protected override void OnSaveDataCommandExecuted(object parameter)
        {
            if (IsDuplicated)
            {
                MessageBox.Show("Названия должны быть уникальными.", "Ошибка", MessageBoxButton.OK);
                return;
            }
            AcceptChanges();
            Status = SaveStatus.Saved;
        }

        #endregion

        #endregion

        protected override bool IsItemDuplicate(Club first, Club second) => first.EqualProp(second, "Name") && first.City.EqualProp(second.City);

        protected override void LoadData()
        {
            Cities = new List<City>() { emptyCity };
            Cities.AddRange(DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City;"));

            DataBaseConnection.SQLiteConnection.Query<Club>("SELECT * FROM Club;").ForEach(club =>
            {
                club.City = Cities.First(c => c.Id == club.CityId);
                Items.Add(new EditableModel<Club>(club) { IsUnremovable = IsUsedInTournaments(club.Id) });
            });
        }

        protected override Club CreateItem() => new Club { City = emptyCity };

        public void UpdateCities()
        {
            Cities.Clear();
            Cities.Add(emptyCity);
            Cities.AddRange(DataBaseConnection.SQLiteConnection.Query<City>("SELECT * FROM City;"));
            Items.ToList().ForEach(item =>
            {
                if (item.Backup != null)
                    item.Backup.City = Cities.First(city => city.Id == item.Backup.CityId);
                item.Data.City = Cities.Find(city => city.Id == item.Data.CityId);
            });
        }

        public override void AcceptChanges()
        {
            RemovedItems.ConvertAll(c => c.Backup).ForEach(club =>
            {
                DataBaseConnection.SQLiteConnection.Execute("UPDATE Judge SET club_id=0 WHERE club_id=?;", club.Id);
                DataBaseConnection.SQLiteConnection.Delete<Club>(club.Id);
            });
            RemovedItems.Clear();

            Items.ToList().ForEach(clubChObj =>
            {
                if (clubChObj.IsNew)
                    DataBaseConnection.SQLiteConnection.Insert(clubChObj.Data);
                else if (clubChObj.IsChanged)
                    DataBaseConnection.SQLiteConnection.Update(clubChObj.Data);
                clubChObj.AcceptChanges();
            });

            IsChanged = false;
        }

        bool IsUsedInTournaments(int clubId) => DataBaseConnection.SQLiteConnection.Query<Member>("SELECT * FROM Member WHERE id IN (SELECT member_id FROM GroupMember WHERE group_id IN (SELECT DISTINCT id FROM [Group] WHERE tournament_category_id IN (SELECT id FROM TournamentCategory WHERE tournament_id IN (SELECT id FROM Tournament WHERE id<>?)) OR tournament_nomination_id IN (SELECT id FROM TournamentNomination WHERE tournament_id IN (SELECT id FROM Tournament WHERE id<>?)))) AND club_id=?;", MainViewModel.Tournament.Id, MainViewModel.Tournament.Id, clubId).Count != 0
            || DataBaseConnection.SQLiteConnection.Query<Judge>("SELECT * FROM Judge WHERE id IN (SELECT judge_id FROM TournamentJudge WHERE tournament_id IN (SELECT id FROM Tournament WHERE id<>?)) AND club_id=?;", MainViewModel.Tournament.Id, clubId).Count != 0;
    }
}