﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Windows.Controls;
using TournamentSoftware.Models;
using TournamentSoftware.Types;
using TournamentSoftware.Windows.Main.Pages;

namespace TournamentSoftware.ViewModels
{
    class MainViewModel
    {
        public static event EventHandler<PropertyChangedEventArgs> StaticPropertyChanged;

        static Page selectedPage;
        static StartPage startPage;
        static MemberPage memberPage;
        static SubgroupPage subgroupPage;
        static TournamentPage tournamentPage;

        static MainViewModel()
        {
            SwitchPage(MainPageType.Start);
        }

        public static Tournament Tournament { get; set; }

        public static int SelectedSubgroupId { get; set; }

        public static int SelectedBattleProtocolId { get; set; }

        public static Page SelectedPage
        {
            get => selectedPage;
            set => Set(ref selectedPage, value);
        }

        public static void SwitchPage(MainPageType mainPageType)
        {
            switch (mainPageType)
            {
                case MainPageType.Start:
                    if (startPage == null)
                        startPage = new StartPage();
                    SelectedPage = startPage;
                    memberPage = null;
                    break;
                case MainPageType.MemberRegistration:
                    if (memberPage == null)
                        memberPage = new MemberPage();
                    SelectedPage = memberPage;
                    subgroupPage = null;
                    break;
                case MainPageType.SubgroupsFormation:
                    if (subgroupPage == null)
                        subgroupPage = new SubgroupPage();
                    SelectedPage = subgroupPage;
                    tournamentPage = null;
                    break;
                case MainPageType.TournamentGrid:
                    if (tournamentPage == null)
                        tournamentPage = new TournamentPage();
                    SelectedPage = tournamentPage;
                    break;
            }
        }

        public static void OnPropertyChanged([CallerMemberName] string propetyName = null) => StaticPropertyChanged?.Invoke(null, new PropertyChangedEventArgs(propetyName));

        public static bool Set<T>(ref T field, T value, [CallerMemberName] string propetyName = null)
        {
            if (Equals(field, value))
                return false;
            field = value;
            OnPropertyChanged(propetyName);
            return true;
        }
    }
}