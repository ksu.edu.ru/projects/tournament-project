using System.Collections;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Fields
{
    public partial class MultiSelectComboBox : UserControl
    {
        ObservableCollection<Node> _nodeList;
        public MultiSelectComboBox()
        {
            InitializeComponent();
            _nodeList = new ObservableCollection<Node>();
        }

        #region Dependency Properties

        public static readonly DependencyProperty ItemsSourceProperty =
             DependencyProperty.Register("ItemsSource", typeof(IList), typeof(MultiSelectComboBox), new FrameworkPropertyMetadata(null,
        new PropertyChangedCallback(MultiSelectComboBox.OnItemsSourceChanged)));

        public static readonly DependencyProperty SelectedItemsProperty =
             DependencyProperty.Register("SelectedItems", typeof(IList), typeof(MultiSelectComboBox), new FrameworkPropertyMetadata(null,
         new PropertyChangedCallback(MultiSelectComboBox.OnSelectedItemsChanged)));

        public static readonly DependencyProperty TextProperty =
           DependencyProperty.Register("Text", typeof(string), typeof(MultiSelectComboBox), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty DefaultTextProperty =
            DependencyProperty.Register("DefaultText", typeof(string), typeof(MultiSelectComboBox), new UIPropertyMetadata(string.Empty));

        public static readonly DependencyProperty DisplayMemberPathProperty =
            DependencyProperty.Register("DisplayMemberPath", typeof(string), typeof(MultiSelectComboBox), new UIPropertyMetadata(string.Empty));

        public IList ItemsSource
        {
            get => (IList)GetValue(ItemsSourceProperty);
            set => SetValue(ItemsSourceProperty, value);
        }

        public IList SelectedItems
        {
            get => (IList)GetValue(SelectedItemsProperty);
            set => SetValue(SelectedItemsProperty, value);
        }

        public string Text
        {
            get => (string)GetValue(TextProperty);
            set => SetValue(TextProperty, value);
        }

        public string DefaultText
        {
            get => (string)GetValue(DefaultTextProperty);
            set => SetValue(DefaultTextProperty, value);
        }

        public string DisplayMemberPath
        {
            get => (string)GetValue(DisplayMemberPathProperty);
            set => SetValue(DisplayMemberPathProperty, value);
        }

        #endregion

        #region Events

        static void OnItemsSourceChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiSelectComboBox control = (MultiSelectComboBox)d;
            if (control.ItemsSource is INotifyCollectionChanged)
                (control.ItemsSource as INotifyCollectionChanged).CollectionChanged += control.ItemsChange;
            control.DisplayInControl();
        }

        static void OnSelectedItemsChanged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            MultiSelectComboBox control = (MultiSelectComboBox)d;
            control.SelectNodes();
            control.SetAll();
            control.SetText();
        }

        #endregion

        #region Methods
        void CheckBox_Click(object sender, RoutedEventArgs e)
        {
            CheckBox clickedBox = (CheckBox)sender;
            if (clickedBox.Content == "Все")
            {
                foreach (Node node in _nodeList) node.IsSelected = (bool)clickedBox.IsChecked;
                SelectedItems.Clear();
                if ((bool)clickedBox.IsChecked)
                    foreach (object obj in ItemsSource)
                        SelectedItems.Add(obj);
                SetText();
                return;
            }
            Node clickedNode = clickedBox.DataContext as Node;
            if (SelectedItems.IndexOf(clickedNode.Item) == -1)
                SelectedItems.Add(clickedNode.Item);
            else
                SelectedItems.Remove(clickedNode.Item);
            SetAll();
            SetText();
        }

        void SelectNodes()
        {
            _nodeList.ToList().ForEach(n => n.IsSelected = false);
            foreach (object item in SelectedItems)
            {
                Node node = _nodeList.FirstOrDefault(i => i.Item == item);
                if (node != null)
                    node.IsSelected = true;
            }
        }

        void SetText()
        {
            if (SelectedItems != null && SelectedItems.Count != 0)
            {
                StringBuilder displayText = new StringBuilder();
                foreach (Node s in _nodeList)
                {
                    if (s.IsSelected && s.Title == "Все")
                    {
                        displayText.Append("Все");
                        break;
                    }
                    else if (s.IsSelected && s.Title != "Все")
                    {
                        displayText.Append(s.Title);
                        displayText.Append(',');
                    }
                }  
                Text = displayText.ToString().TrimEnd(new char[] { ',' });
            } 
            else
            {
                _nodeList.ToList().ForEach(item => item.IsSelected = false);
                Text = DefaultText;
            }
        }

        void DisplayInControl()
        {
            _nodeList.Clear();
            if (ItemsSource.Count > 0)
                _nodeList.Add(new Node() { Title = "Все" });
            foreach (object item in ItemsSource)
            {
                if(item is INotifyPropertyChanged)
                    (item as INotifyPropertyChanged).PropertyChanged += SetTitle;
                Node node = new Node
                {
                    Item = item,
                    Title = GetStringFromProperty(item, DisplayMemberPath)
                };
                _nodeList.Add(node);
            }
            MultiSelectCombo.ItemsSource = _nodeList;
        }

        void ItemsChange(object sender, NotifyCollectionChangedEventArgs e)
        {
            if (ItemsSource.Count > 0 && !_nodeList.ToList().Exists(n => n.Title == "Все"))
                _nodeList.Insert( 0, new Node { Title = "Все" });
            if (e.Action == NotifyCollectionChangedAction.Reset)
                _nodeList.Clear();
            if (e.Action == NotifyCollectionChangedAction.Add)
            {
                foreach (object item in e.NewItems)
                {
                    if (item is INotifyPropertyChanged)
                        (item as INotifyPropertyChanged).PropertyChanged += SetTitle;
                    _nodeList.Add(new Node
                    {
                        Item = item,
                        Title = GetStringFromProperty(item, DisplayMemberPath),
                        IsSelected = false
                    });
                }
            }
            if (e.Action == NotifyCollectionChangedAction.Remove)
            {
                foreach (object item in e.OldItems)
                {
                    if (item is INotifyPropertyChanged)
                        (item as INotifyPropertyChanged).PropertyChanged -= SetTitle;
                    _nodeList.Remove(_nodeList.ToList().Find(n => n.Item == item));
                }
            }
            SetAll();
            SetText();
        }

        void SetTitle(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == DisplayMemberPath)
            {
                Node node = _nodeList.FirstOrDefault(n => n.Item == sender);
                if (node != null)
                    node.Title = GetStringFromProperty(sender, DisplayMemberPath);
                SetText();
            }
        }

        string GetStringFromProperty(object obj, string propertyName)
        {
            return obj.GetType()?.GetProperty(propertyName)?.GetValue(obj)?.ToString() ?? obj.ToString();
        }

        void SetAll()
        {
            if (_nodeList.Count > 0)
                if (_nodeList.All(n => n.IsSelected || n.Title == "Все"))
                    _nodeList.FirstOrDefault(n => n.Title == "Все").IsSelected = true;
                else
                    _nodeList.FirstOrDefault(n => n.Title == "Все").IsSelected = false;
        }

        #endregion
    }

    class Node : NotifyPropertyChangedObject
    {
        object item;
        string title;
        bool isSelected;

        #region Properties

        public string Title
        {
            get => title;
            set => Set(ref title, value);
        }
        public object Item
        {
            get => item;
            set => Set(ref item, value);
        }
        public bool IsSelected
        {
            get => isSelected;
            set => Set(ref isSelected, value);
        }

        #endregion
    }
}
