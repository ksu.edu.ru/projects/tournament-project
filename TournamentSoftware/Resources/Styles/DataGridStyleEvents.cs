﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace TournamentSoftware.Resources.Styles
{
    public partial class DataGridStyleEventsv : ResourceDictionary
    {
        void NumberOnlyTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int caretIndex = (sender as TextBox).CaretIndex;
            (sender as TextBox).Text = Regex.Replace((sender as TextBox).Text, "[^0-9]+", "");
            (sender as TextBox).CaretIndex = caretIndex;
        }
    }
}
