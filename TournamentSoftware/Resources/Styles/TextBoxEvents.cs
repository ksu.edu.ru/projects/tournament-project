﻿using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;

namespace TournamentSoftware.Resources.Styles
{
    public partial class TextBoxEvents : ResourceDictionary
    {
        public TextBoxEvents() => InitializeComponent();

        void TextBox_LostFocus(object sender, RoutedEventArgs e) => (sender as TextBox).Text.Trim();

        void BaseNumberTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            int caretIndex = (sender as TextBox).CaretIndex;
            (sender as TextBox).Text = Regex.Replace((sender as TextBox).Text, "[^0-9]+", "");
            (sender as TextBox).CaretIndex = caretIndex;
        }
    }
}
