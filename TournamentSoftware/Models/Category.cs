﻿using SQLite;
using System;
using System.Text.RegularExpressions;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("Category")]
    public class Category : ValidateModel, ICloneable, IEquatableProps<Category>
    {
        int id;
        string name;
        bool selectedForCurrentTournament;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("name"), MaxLength(35)]
        public string Name
        {
            get => name;
            set => PreviewSet(ref name, value);
        }

        [Ignore]
        public bool SelectedForCurrentTournament
        {
            get => selectedForCurrentTournament;
            set => Set(ref selectedForCurrentTournament, value);
        }

        public object Clone() => new Category { Id = Id, Name = Name, SelectedForCurrentTournament = SelectedForCurrentTournament };

        public bool EqualProp(Category other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Name";
                        break;
                    case "Name":
                        result = name == other.name;
                        if (propertyName == null && result)
                            goto case "SelectedForCurrentTournament";
                        break;
                    case "SelectedForCurrentTournament":
                        result = selectedForCurrentTournament == other.selectedForCurrentTournament;
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName)
        {
            switch (columnName)
            {
                case "Name":
                    if (string.IsNullOrWhiteSpace(Name))
                        return "Название категоии не может быть пустым";
                    if (Regex.IsMatch(Name, "[^A-Za-zА-Яа-яЁё ]"))
                        return "В названии категоии могут быть только символы латинского и русского алфавитов и пробел";
                    if (Name.Length > 35)
                        return "Название категоии не может быть больше 35 символов";
                    return "";
                default:
                    return "";
            }
        }
    }
}