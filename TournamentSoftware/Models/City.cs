﻿using SQLite;
using System;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("City")]
    public class City : ValidateModel, ICloneable, IEquatableProps<City>
    {
        int id;
        string name;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("name"), MaxLength(25)]
        public string Name
        {
            get => name;
            set => PreviewSet(ref name, value);
        }

        public object Clone() => new City { Id = Id, Name = Name };

        public bool EqualProp(City other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Name";
                        break;
                    case "Name":
                        result = name == other.name;
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName)
        {
            switch (columnName)
            {
                case "Name":
                    if (string.IsNullOrWhiteSpace(Name))
                        return "Название города не может быть пустым";
                    if (Name.Length > 25)
                        return "Название города не может быть больше 25 символов";
                    return "";
                default:
                    return "";
            }
        }
    }
}