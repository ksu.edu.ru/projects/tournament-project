﻿using SQLite;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Models
{
    [Table("JudgeNote")]
    public class JudgeNote : NotifyPropertyChangedObject
    {
        public delegate void scoreChangeHandler();
        public event scoreChangeHandler onScoreChanged;

        int id;
        int number;
        int score;
        int difference;
        int point;
        int countOfWarnings;
        int countOfKnockdowns;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("number")]
        public int Number
        {
            get => number;
            set => Set(ref number, value);
        }

        [Column("point")]
        public int Point
        {
            get => point;
            set => Set(ref point, value);
        }

        [Column("difference")]
        public int Difference
        {
            get => difference;
            set => Set(ref difference, value);
        }

        [Column("score")]
        public int Score
        {
            get => score;
            set
            {
                Set(ref score, value);
                onScoreChanged?.Invoke();
            }
        }

        [Column("count_of_warnings")]
        public int CountOfWarnings
        {
            get => countOfWarnings;
            set => Set(ref countOfWarnings, value);
        }

        [Column("count_of_knockdowns")]
        public int CountOfKnockdowns
        {
            get => countOfKnockdowns;
            set => Set(ref countOfKnockdowns, value);
        }

        [Column("tournament_judge_id"), Indexed]
        public int TournamentJudgeId { get; set; }

        [Column("round_fighter_id"), Indexed]
        public int RoundFighterId { get; set; }
    }
}
