﻿using SQLite;
using System;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("SubgroupMember")]
    public class SubgroupMember : ValidateModel, ICloneable, IEquatableProps<SubgroupMember>
    {
        int id;
        int number;
        int subgroupId;
        int memberId;
        Member member;
        bool isSelectedForFinal;
        int winCount;
        int defeatCount;
        int totalScore;
        string warnings;
        int countOfWarnings;
        int rating;
        bool isDisqualified;
        
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("number")]
        public int Number
        {
            get => number;
            set => Set(ref number, value);
        }

        [Column("subgroup_id"), Indexed]
        public int SubgroupId
        {
            get => subgroupId;
            set => Set(ref subgroupId, value);
        }

        [Column("member_id"), Indexed]
        public int MemberId
        {
            get => memberId;
            set => Set(ref memberId, value);
        }

        [Ignore]
        public Member Member
        {
            get => member;
            set
            {
                if (Set(ref member, value))
                    MemberId = value?.Id ?? 0;
            }
        }

        [Column("warnings")]
        public string Warnings
        {
            get => warnings;
            set => Set(ref warnings, value);
        }

        [Column("is_disqualified")]
        public bool IsDisqualified
        {
            get => isDisqualified;
            set
            {
                if (Set(ref isDisqualified, value) && value)
                    IsSelectedForFinal = false;
            }
        }

        [Ignore]
        public bool IsSelectedForFinal
        {
            get => isSelectedForFinal;
            set => Set(ref isSelectedForFinal, value);
        }

        [Ignore]
        public int WinCount
        {
            get => winCount;
            set => Set(ref winCount, value);
        }

        [Ignore]
        public int DefeatCount
        {
            get => defeatCount;
            set => Set(ref defeatCount, value);
        }

        [Ignore]
        public int TotalScore
        {
            get => totalScore;
            set => Set(ref totalScore, value);
        }

        [Ignore]
        public int CountOfWarnings
        {
            get => countOfWarnings;
            set => Set(ref countOfWarnings, value);
        }

        [Ignore]
        public int Rating
        {
            get => rating;
            set => Set(ref rating, value);
        }

        public object Clone() => new SubgroupMember { MemberId = MemberId, IsSelectedForFinal = IsSelectedForFinal, Member = Member, WinCount = WinCount, DefeatCount = DefeatCount, TotalScore = TotalScore, Warnings = Warnings, CountOfWarnings = CountOfWarnings, Rating = Rating, IsDisqualified = IsDisqualified, Id = Id, SubgroupId = SubgroupId, Number = Number };

        public bool CheckValid() => memberId != 0;

        public bool EqualProp(SubgroupMember other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Warnings":
                        result = warnings == other.warnings;
                        if (propertyName == null && result)
                            goto case "IsSelectedForFinal";
                        break;
                    case "IsSelectedForFinal":
                        result = isSelectedForFinal == other.isSelectedForFinal;
                        if (propertyName == null && result)
                            goto case "IsDisqualified";
                        break;
                    case "IsDisqualified":
                        result = isDisqualified == other.isDisqualified;
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName) => "";
    }
}
