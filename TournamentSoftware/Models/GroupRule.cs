﻿using SQLite;

namespace TournamentSoftware.Models
{
    [Table("GroupRule")]
    public class GroupRule
    {
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("name"), MaxLength(35)]
        public string Name { get; set; }
    }
}
