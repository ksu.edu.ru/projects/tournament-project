﻿using SQLite;

namespace TournamentSoftware.Models
{
    [Table("TournamentJudge")]
    public class TournamentJudge
    {
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("tournament_id"), Indexed]
        public int TournamentId { get; set; }

        [Column("judge_id"), Indexed]
        public int JudgeId { get; set; }

        [Ignore]
        public Judge Judge { get; set; }
    }
}