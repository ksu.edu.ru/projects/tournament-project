﻿using SQLite;
using System.Collections.ObjectModel;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Models
{
    [Table("Phase")]
    public class Phase : NotifyPropertyChangedObject
    {
        int id;
        int number;
        int subgroupId;
        SubgroupMember extraMemberSubgroup;
        ObservableCollection<BattleProtocol> battleProtocols;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("number")]
        public int Number
        {
            get => number;
            set => Set(ref number, value);
        }

        [Column("subgroup_id")]
        public int SubgroupId
        {
            get => subgroupId;
            set => Set(ref subgroupId, value);
        }

        [Column("extra_subgroup_member_id")]
        public int ExtraSubgroupMemberId { get; set; }

        [Ignore]
        public SubgroupMember ExtraMemberSubgroup
        {
            get => extraMemberSubgroup;
            set
            {
                if (Set(ref extraMemberSubgroup, value))
                    ExtraSubgroupMemberId = extraMemberSubgroup?.Id ?? 0;
            }
        }

        [Ignore]
        public ObservableCollection<BattleProtocol> BattleProtocols
        {
            get => battleProtocols;
            set => Set(ref battleProtocols, value);
        }
    }
}
