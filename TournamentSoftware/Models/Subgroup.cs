﻿using SQLite;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("Subgroup")]
    public class Subgroup : ValidateModel, IEquatableProps<Subgroup>, ICloneable
    {
        int id;
        string name;
        int groupId;
        bool isRulesFailed;
        FightSystem fightSystem;
        ObservableCollection<Member> members;
        ObservableCollection<Phase> phases;
        bool hasFinalists;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("name")]
        public string Name
        {
            get => name;
            set => Set(ref name, value);
        }

        [Column("fight_system_id")]
        public int FightSystemId { get; set; }

        [Column("group_id"), Indexed]
        public int GroupId
        {
            get => groupId;
            set => Set(ref groupId, value);
        }

        [Ignore]
        public bool IsRulesFailed
        {
            get => isRulesFailed;
            set => Set(ref isRulesFailed, value);
        }

        [Ignore]
        public FightSystem FightSystem
        {
            get => fightSystem;
            set
            {
                if (Set(ref fightSystem, value))
                    FightSystemId = value?.Id ?? 0;
            }
        }

        [Ignore]
        public ObservableCollection<Member> Members
        {
            get => members;
            set => Set(ref members, value);
        }

        [Ignore]
        public ObservableCollection<Phase> Phases
        {
            get => phases;
            set => Set(ref phases, value);
        }

        [Ignore]
        public bool HasFinalists
        {
            get => hasFinalists;
            set => Set(ref hasFinalists, value);
        }

        public bool CheckValid() => members.Count > 1;

        public object Clone() => new Subgroup { id = id, name = name, groupId = groupId, fightSystem = fightSystem, members = members == null ? members : new ObservableCollection<Member>(members), phases = phases == null ? null : new ObservableCollection<Phase>(phases), hasFinalists = hasFinalists, isRulesFailed = isRulesFailed };

        public bool EqualProp(Subgroup other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Name";
                        break;
                    case "Name":
                        result = name == other.name;
                        if (propertyName == null && result)
                            goto case "GroupId";
                        break;
                    case "GroupId":
                        result = groupId == other.groupId;
                        if (propertyName == null && result)
                            goto case "IsRulesFailed";
                        break;
                    case "IsRulesFailed":
                        result = isRulesFailed == other.isRulesFailed;
                        if (propertyName == null && result)
                            goto case "FightSystem";
                        break;
                    case "FightSystem":
                        result = fightSystem?.Id == other.fightSystem?.Id;
                        if (propertyName == null && result)
                            goto case "Members";
                        break;
                    case "Members":
                        result = members == null && other.members == null || members?.Count == other.members?.Count && members.All(s => other.members.Any(sg => sg.Equals(s)));
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName) =>  "";
    }
}