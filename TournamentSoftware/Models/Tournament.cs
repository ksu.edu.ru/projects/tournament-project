﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text.RegularExpressions;
using SQLite;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Models
{
    [Table("Tournament")]
    public class Tournament : NotifyPropertyChangedObject, IDataErrorInfo
    {
        int id;
        string name;
        DateTime date;
        bool registrationCompleted;
        bool subgroupsFormationCompleted;
        bool fightsCompleted;

        string error;
        Dictionary<string, List<string>> nameToErrors = new Dictionary<string, List<string>>();

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("name"), MaxLength(35)]
        public string Name
        {
            get => name;
            set => Set(ref name, value);
        }

        [Column("date")]
        public DateTime Date
        {
            get => date;
            set => Set(ref date, value);
        }

        [Column("registration_completed")]
        public bool RegistrationCompleted
        {
            get => registrationCompleted;
            set => Set(ref registrationCompleted, value);
        }

        [Column("subgroups_formation_completed")]
        public bool SubgroupsFormationCompleted
        {
            get => subgroupsFormationCompleted;
            set => Set(ref subgroupsFormationCompleted, value);
        }

        [Column("fights_completed")]
        public bool FightsCompleted
        {
            get => fightsCompleted;
            set => Set(ref fightsCompleted, value);
        }

        [Ignore]
        public string Error
        {
            get => error;
            set { error = value ?? nameToErrors.Values.ToList().Find(errors => errors.Count > 0)?.First(); OnPropertyChanged(); }
        }

        public string this[string propertyName]
        {
            get
            {
                if (!nameToErrors.ContainsKey(propertyName))
                    nameToErrors.Add(propertyName, null);
                List<string> errors = new List<string>();

                switch (propertyName)
                {
                    case "Name":
                        if (string.IsNullOrWhiteSpace(Name))
                            errors.Add("Название турнира не должно быть пустым");
                        if (Name?.Length > 35)
                            errors.Add("Максимальная длина названия турнира - 35 символов");
                        if (Name != null && !Regex.IsMatch(Name, "^[0-9a-zA-Zа-яА-Яа-ёЁ\"_ ]+$"))
                            errors.Add("Название турнира может содержать только русские или английские символы, цифры, кавычки и знак подчерк _.");
                        break;
                    default:
                        return null;
                }

                if (errors.Count > 0)
                {
                    nameToErrors[propertyName] = errors;
                    return Error = errors.First();
                } else
                {
                    nameToErrors.Remove(propertyName);
                    return Error = null;
                }
            }
        }
    }
}