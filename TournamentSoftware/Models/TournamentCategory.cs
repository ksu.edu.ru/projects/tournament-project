﻿using SQLite;

namespace TournamentSoftware.Models
{
    [Table("TournamentCategory")]
    public class TournamentCategory
    {
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("tournament_id"), Indexed]
        public int TournamentId { get; set; }

        [Column("category_id"), Indexed]
        public int CategoryId { get; set; }
    }
}