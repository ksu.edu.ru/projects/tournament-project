﻿using SQLite;
using System;
using System.Text.RegularExpressions;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("Judge")]
    public class Judge : ValidateModel, ICloneable, IEquatableProps<Judge>
    {
        int id;
        string name;
        string surname;
        string patronymic;
        Club club;
        City city;
        bool selectedForCurrentTournament;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("surname"), MaxLength(25)]
        public string Surname
        {
            get => surname;
            set => PreviewSet(ref surname, value);
        }

        [Column("name"), MaxLength(15)]
        public string Name
        {
            get => name;
            set => PreviewSet(ref name, value);
        }

        [Column("patronymic"), MaxLength(35)]
        public string Patronymic
        {
            get => patronymic;
            set => PreviewSet(ref patronymic, value);
        }

        [Column("club_id"), Indexed]
        public int ClubId { get; set; }

        [Ignore]
        public Club Club
        {
            get => club;
            set
            {
                if (Set(ref club, value))
                    ClubId = club?.Id ?? 0;
            }
        }

        [Column("city_id"), Indexed]
        public int CityId { get; set; }

        [Ignore]
        public City City
        {
            get => city;
            set
            {
                if (Set(ref city, value))
                    CityId = city?.Id ?? 0;
            }
        }

        [Ignore]
        public bool SelectedForCurrentTournament
        {
            get => selectedForCurrentTournament;
            set => Set(ref selectedForCurrentTournament, value);
        }

        public bool CheckValid() => !string.IsNullOrWhiteSpace(name) && !string.IsNullOrWhiteSpace(surname) && city != null && club != null;

        public object Clone() => new Judge { Id = Id, Name = Name, Surname = Surname, Patronymic = Patronymic, Club = Club, City = City, SelectedForCurrentTournament = SelectedForCurrentTournament };

        public bool EqualProp(Judge other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Name";
                        break;
                    case "Name":
                        result = name == other.name;
                        if (propertyName == null && result)
                            goto case "Surname";
                        break;
                    case "Surname":
                        result = surname == other.surname;
                        if (propertyName == null && result)
                            goto case "Patronymic";
                        break;
                    case "Patronymic":
                        result = patronymic == other.patronymic;
                        if (propertyName == null && result)
                            goto case "SelectedForCurrentTournament";
                        break;
                    case "SelectedForCurrentTournament":
                        result = selectedForCurrentTournament == other.selectedForCurrentTournament;
                        if (propertyName == null && result)
                            goto case "Club";
                        break;
                    case "Club":
                        result = club?.Id == other.club?.Id;
                        if (propertyName == null && result)
                            goto case "City";
                        break;
                    case "City":
                        result = city?.Id == other.city?.Id;
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName)
        {
            switch (columnName)
            {
                case "Name":
                    if (string.IsNullOrWhiteSpace(Name))
                        return "Имя судьи не может быть пустым";
                    if (Name.Length < 3 || Name.Length > 15)
                        return "Имя судьи может составлять от 2 до 15 симвлоа";
                    if (string.IsNullOrWhiteSpace(Name))
                        return "Назнавине судьи не может быть пустым";
                    if (Regex.IsMatch(Name, "[^А-Яа-яЁё <>,'-]"))
                        return "В имени присутствуют недопустимые символы (цифры, буквы латинского алфавита или спец. символы не из [пробел]<>,'- набора)";
                    return "";
                case "Surname":
                    if (string.IsNullOrWhiteSpace(Surname))
                        return "Фамилия судьи не может быть пустым";
                    if (Surname.Length < 2 || Surname.Length > 25)
                        return "Фамилия судьи может составлять от 2 до 25 симвлоа.";
                    if (Regex.IsMatch(Surname, "[^А-Яа-яЁё]"))
                        return "В фамилии присутствуют недопустимые символы (цифры, буквы латинского алфавита или спец. символы не из <>,'- набора)";
                    return "";
                case "Patronymic":
                    if (Patronymic.Length > 35)
                        return "Отчество судьи может составлять до 35 симвлоа.";
                    if (Regex.IsMatch(Patronymic, "[^А-Яа-яЁё]"))
                        return "В фамилии присутствуют недопустимые символы (цифры, буквы латинского алфавита или спец. символы не из <>,'- набора)";
                    return "";
                case "Club":
                    if (Club == null)
                        return "Не выбран клуб";
                    return "";
                case "City":
                    if (City == null)
                        return "Не выбран город";
                    return "";
                default:
                    return "";
            }
        }
    }
}