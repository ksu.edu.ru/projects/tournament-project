﻿using SQLite;
using System.Collections.Generic;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Models
{
    [Table("BattleProtocol")]
    public class BattleProtocol : NotifyPropertyChangedObject
    {
        int id;
        int number;
        int customNumber;
        string customSubgroup;
        List<Fighter> fighters;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("number")]
        public int Number
        {
            get => number;
            set => Set(ref number, value);
        }

        [Column("phase_id"), Indexed]
        public int PhaseId { get; set; }

        [Column("custom_number")]
        public int CustomNumber
        {
            get => customNumber;
            set => Set(ref customNumber, value);
        }

        [Column("custom_subgroup")]
        public string CustomSubgroup
        {
            get => customSubgroup;
            set => Set(ref customSubgroup, value);
        }
        
        [Ignore]
        public List<Fighter> Fighters
        {
            get => fighters;
            set => Set(ref fighters, value);
        }
    }
}
