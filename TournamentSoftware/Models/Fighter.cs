﻿using SQLite;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Types;

namespace TournamentSoftware.Models
{
    [Table("Fighter")]
    public class Fighter : NotifyPropertyChangedObject
    {
        int id;
        string note;
        string remark;
        int totalScore;
        int subgroupMemberId;
        SubgroupMember subgroupMember;
        FighterResultType result;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("note")]
        public string Note
        {
            get => note;
            set => Set(ref note, value);
        }

        [Column("remark")]
        public string Remark
        {
            get => remark;
            set => Set(ref remark, value);
        }

        [Column("total_score")]
        public int TotalScore
        {
            get => totalScore;
            set => Set(ref totalScore, value);
        }

        [Column("battle_protocol_id"), Indexed]
        public int BattleProtocolId { get; set; }

        [Column("subgroup_member_id"), Indexed]
        public int SubgroupMemberId
        {
            get => subgroupMemberId;
            set => Set(ref subgroupMemberId, value);
        }

        [Ignore]
        public SubgroupMember SubgroupMember
        {
            get => subgroupMember;
            set
            {
                if (Set(ref subgroupMember, value))
                    SubgroupMemberId = value?.Id ?? 0;
            }
        }

        [Ignore]
        public FighterResultType Result
        {
            get => result;
            set => Set(ref result, value);
        }
    }
}
