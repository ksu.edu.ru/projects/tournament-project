﻿using SQLite;

namespace TournamentSoftware.Models
{
    [Table("GroupMember")]
    public class GroupMember
    {
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("group_id"), Indexed]
        public int GroupId { get; set; }

        [Column("member_id"), Indexed]
        public int MemberId { get; set; }
    }
}