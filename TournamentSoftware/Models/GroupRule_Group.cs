﻿using SQLite;

namespace TournamentSoftware.Models
{
    [Table("GroupRule_Group")]
    public class GroupRule_Group
    {
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("group_id"), Indexed]
        public int GroupId { get; set; }

        [Column("group_rule_id"), Indexed]
        public int GroupRuleId { get; set; }
    }
}
