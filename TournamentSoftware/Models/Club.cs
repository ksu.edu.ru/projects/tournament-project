﻿using SQLite;
using System;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("Club")]
    public class Club : ValidateModel, ICloneable, IEquatableProps<Club>
    {
        int id;
        string name;
        int cityId;
        City city;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("name"), MaxLength(35)]
        public string Name
        {
            get => name;
            set => PreviewSet(ref name, value);
        }

        [Column("city_id"), Indexed]
        public int CityId
        {
            get => cityId;
            set => Set(ref cityId, value);
        }

        [Ignore]
        public City City
        {
            get => city;
            set
            {
                if (PreviewSet(ref city, value))
                    CityId = city?.Id ?? 0;
            }
        }

        [Ignore]
        public string DisplayName => $"{name}{(city != null ? $" - {city.Name}" : "")}";

        public bool CheckValid() => !string.IsNullOrWhiteSpace(name) && city != null;

        public object Clone() => new Club { Id = Id, Name = Name, City = City };

        public bool EqualProp(Club other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Name";
                        break;
                    case "Name":
                        result = name == other.name;
                        if (propertyName == null && result)
                            goto case "City";
                        break;
                    case "City":
                        result = city?.Id == other.city?.Id;
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName)
        {
            switch (columnName)
            {
                case "Name":
                    if (string.IsNullOrWhiteSpace(Name))
                        return "Название клуба не может быть пустым";
                    if (Name.Length > 35)
                        return "Название клуба не может быть больше 35 символов";
                    return "";
                case "City":
                    if (city == null)
                        return "Не выбран город";
                    return "";
                default:
                    return "";
            }
        }
    }
}