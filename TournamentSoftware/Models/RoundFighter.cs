﻿using SQLite;
using System.Collections.ObjectModel;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Models
{
    [Table("RoundFighter")]
    public class RoundFighter : NotifyPropertyChangedObject
    {
        int totalScore;
        Fighter fighter;
        int fighterId;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("total_score")]
        public int TotalScore
        {
            get => totalScore;
            set => Set(ref totalScore, value);
        }

        [Column("round_id"), Indexed]
        public int RoundId { get; set; }

        [Column("fighter_id"), Indexed]
        public int FighterId
        {
            get => fighterId;
            set => Set(ref fighterId, value);
        }

        [Ignore]
        public ObservableCollection<JudgeNote> JudgeNotes { get; set; }

        [Ignore]
        public Fighter Fighter
        {
            get => fighter;
            set
            {
                if (Set(ref fighter, value))
                    FighterId = fighter?.Id ?? 0;
            }
        }
    }
}