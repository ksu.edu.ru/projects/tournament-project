﻿using SQLite;
using System.Collections.Generic;
using TournamentSoftware.Infrastructure;

namespace TournamentSoftware.Models
{
    [Table("Round")]
    public class Round : NotifyPropertyChangedObject
    {
        int number;
        List<RoundFighter> roundFighters;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("number")]
        public int Number
        {
            get => number;
            set => Set(ref number, value);
        }

        [Column("battle_protocol_id"), Indexed]
        public int BattleProtocolId { get; set; }

        [Ignore]
        public List<RoundFighter> RoundFighters
        {
            get => roundFighters;
            set => Set(ref roundFighters, value);
        }
    }
}