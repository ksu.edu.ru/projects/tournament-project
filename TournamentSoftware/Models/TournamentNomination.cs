﻿using SQLite;

namespace TournamentSoftware.Models
{
    [Table("TournamentNomination")]
    public class TournamentNomination
    {
        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id { get; set; }

        [Column("tournament_id"), Indexed]
        public int TournamentId { get; set; }

        [Column("nomination_id"), Indexed]
        public int NominationId { get; set; }
    }
}