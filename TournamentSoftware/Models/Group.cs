﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("Group")]
    public class Group : ValidateModel, ICloneable, IEquatableProps<Group>
    {
        int id;
        Category category;
        int tournamentCategoryId;
        Nomination nomination;
        int tournamentNominationId;
        string name;
        List<GroupRule> groupRules;
        ObservableCollection<Subgroup> subgroups;
        List<Member> members;
        bool isFinalEnabled;
        bool isFightsOver;
        bool isFinalCreated;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("tournament_category_id"), Indexed]
        public int TournamentCategoryId
        {
            get => tournamentCategoryId;
            set => Set(ref tournamentCategoryId, value);
        }

        [Column("tournament_nomination_id"), Indexed]
        public int TournamentNominationId
        {
            get => tournamentNominationId;
            set => Set(ref tournamentNominationId, value);
        }

        [Ignore]
        public bool IsFinalEnabled
        {
            get => isFinalEnabled;
            set => Set(ref isFinalEnabled, value);
        }

        [Ignore]
        public bool IsFightsOver
        {
            get => isFightsOver;
            set => Set(ref isFightsOver, value);
        }

        [Ignore]
        public bool IsFinalCreated
        {
            get => isFinalCreated;
            set => Set(ref isFinalCreated, value);
        }

        [Ignore]
        public Category Category
        {
            get => category;
            set
            {
                category = value;
                if (value != null && Nomination != null)
                    Name = value.Name + " - " + Nomination.Name;
            }
        }

        [Ignore]
        public Nomination Nomination
        {
            get => nomination;
            set
            {
                nomination = value;
                if (Category != null && value != null)
                    Name = Category.Name + " - " + value.Name; //FIXME
            }
        }

        [Ignore]
        public string Name
        {
            get => name;
            set => Set(ref name, value);
        }

        [Ignore]
        public List<GroupRule> GroupRules
        {
            get => groupRules;
            set => Set(ref groupRules, value);
        }

        [Ignore]
        public ObservableCollection<Subgroup> Subgroups
        {
            get => subgroups;
            set => Set(ref subgroups, value);
        }

        [Ignore]
        public List<Member> Members
        {
            get => members;
            set => Set(ref members, value);
        }

        public bool CheckValid() => subgroups?.Count > 0;

        public object Clone() => new Group { id = id, tournamentCategoryId = tournamentCategoryId, tournamentNominationId = tournamentNominationId, category = category, nomination = nomination, name = name, groupRules = groupRules == null ? groupRules : new List<GroupRule>(groupRules), subgroups = subgroups == null ? subgroups : new ObservableCollection<Subgroup>(subgroups), members = members == null ? members : new List<Member>(members), isFinalCreated = isFinalCreated, isFightsOver = isFightsOver, isFinalEnabled = isFinalEnabled };

        public bool EqualProp(Group other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Nomination";
                        break;
                    case "Nomination":
                        result = nomination?.Id == other.nomination?.Id;
                        if (propertyName == null && result)
                            goto case "Category";
                        break;
                    case "Category":
                        result = category?.Id == other.category?.Id;
                        if (propertyName == null && result)
                            goto case "GroupRules";
                        break;
                    case "GroupRules":
                        result = groupRules == null && other.groupRules == null || groupRules?.Count == other.groupRules?.Count && groupRules.All(groupRule => other.groupRules.Any(gr => groupRule.Equals(gr)));
                        if (propertyName == null && result)
                            goto case "Subgroups";
                        break;
                    case "Subgroups":
                        result = subgroups == null && other.subgroups == null || subgroups?.Count == other.subgroups?.Count && subgroups.All(s => other.subgroups.Any(sg => sg.Equals(s)));
                        if (propertyName == null && result)
                            goto case "Members";
                        break;
                    case "Members":
                        result = members == null && other.members == null || members?.Count == other.members?.Count && members.All(s => other.members.Any(sg => sg.Equals(s)));
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName) => "";
    }
}