﻿using SQLite;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text.RegularExpressions;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Models
{
    [Table("Member")]
    public class Member : ValidateModel, ICloneable, IEquatableProps<Member>
    {
        int id;
        string name;
        string surname;
        string patronymic;
        string alias;
        bool leader;
        string sex;
        int yearOfBirth;
        City city;
        Club club;
        int height;
        int weight;
        ObservableCollection<Group> groups;
        int commonRating;
        int clubRating;

        [Column("id"), PrimaryKey, AutoIncrement]
        public int Id
        {
            get => id;
            set => Set(ref id, value);
        }

        [Column("surname"), MaxLength(25)]
        public string Surname
        {
            get => surname;
            set => PreviewSet(ref surname, value);
        }

        [Column("name"), MaxLength(15)]
        public string Name
        {
            get => name;
            set => PreviewSet(ref name, value);
        }

        [Column("patronymic"), MaxLength(35)]
        public string Patronymic
        {
            get => patronymic;
            set => Set(ref patronymic, value);
        }

        [Column("alias"), MaxLength(35)]
        public string Alias
        {
            get => alias;
            set => Set(ref alias, value);
        }

        [Column("leader")]
        public bool Leader
        {
            get => leader;
            set => Set(ref leader, value);
        }

        [Column("sex"), MaxLength(1)]
        public string Sex
        {
            get => sex;
            set => Set(ref sex, value);
        }

        [Column("year_of_birth")]
        public int YearOfBirth
        {
            get => yearOfBirth;
            set => PreviewSet(ref yearOfBirth, value);
        }

        [Column("height")]
        public int Height
        {
            get => height;
            set => Set(ref height, value);
        }

        [Column("weight")]
        public int Weight
        {
            get => weight;
            set => Set(ref weight, value);
        }

        [Column("common_rating")]
        public int CommonRating
        {
            get => commonRating;
            set => Set(ref commonRating, value);
        }

        [Column("club_rating")]
        public int ClubRating
        {
            get => clubRating;
            set => Set(ref clubRating, value);
        }

        [Column("club_id"), Indexed]
        public int ClubId { get; set; }

        [Column("city_id"), Indexed]
        public int CityId { get; set; }

        [Ignore]
        public ObservableCollection<Group> Groups
        {
            get => groups;
            set { Set(ref groups, value); OnPropertyChanged(); }
        }

        [Ignore]
        public City City
        {
            get => city;
            set
            {
                if (Set(ref city, value))
                    CityId = city?.Id ?? 0;
            }
        }

        [Ignore]
        public Club Club
        {
            get => club;
            set
            {
                if (Set(ref club, value))
                    ClubId = club?.Id ?? 0;
            }
        }

        public object Clone() => new Member { id = id, name = name, surname = surname, patronymic = patronymic, alias = alias, leader = leader, sex = sex, yearOfBirth = yearOfBirth, height = height, weight = weight, commonRating = commonRating, clubRating = clubRating, ClubId = ClubId, club = club, CityId = CityId, city = city, groups = groups == null ? groups : new ObservableCollection<Group>(groups) };

        public bool EqualProp(Member other, string propertyName = null)
        {
            bool result = other != null;
            if (result)
                switch (propertyName)
                {
                    case null:
                    case "Id":
                        result = id == other.id;
                        if (propertyName == null && result)
                            goto case "Name";
                        break;
                    case "Name":
                        result = name == other.name;
                        if (propertyName == null && result)
                            goto case "Surname";
                        break;
                    case "Surname":
                        result = surname == other.surname;
                        if (propertyName == null && result)
                            goto case "Patronymic";
                        break;
                    case "Patronymic":
                        result = patronymic == other.patronymic;
                        if (propertyName == null && result)
                            goto case "Alias";
                        break;
                    case "Alias":
                        result = alias == other.alias;
                        if (propertyName == null && result)
                            goto case "Leader";
                        break;
                    case "Leader":
                        result = leader == other.leader;
                        if (propertyName == null && result)
                            goto case "Sex";
                        break;
                    case "Sex":
                        result = sex == other.sex;
                        if (propertyName == null && result)
                            goto case "YearOfBirth";
                        break;
                    case "YearOfBirth":
                        result = yearOfBirth == other.yearOfBirth;
                        if (propertyName == null && result)
                            goto case "Height";
                        break;
                    case "Height":
                        result = height == other.height;
                        if (propertyName == null && result)
                            goto case "Weight";
                        break;
                    case "Weight":
                        result = weight == other.weight;
                        if (propertyName == null && result)
                            goto case "CommonRating";
                        break;
                    case "CommonRating":
                        result = commonRating == other.commonRating;
                        if (propertyName == null && result)
                            goto case "ClubRating";
                        break;
                    case "ClubRating":
                        result = clubRating == other.clubRating;
                        if (propertyName == null && result)
                            goto case "Club";
                        break;
                    case "Club":
                        result = club?.Id == other.club?.Id;
                        if (propertyName == null && result)
                            goto case "City";
                        break;
                    case "City":
                        result = city?.Id == other.city?.Id;
                        if (propertyName == null && result)
                            goto case "Groups";
                        break;
                    case "Groups":
                        result = groups == null && other.groups == null || groups?.Count == other.groups?.Count && groups.All(group => other.groups.Any(g => group.Equals(g)));
                        break;
                    default:
                        throw new ArgumentException($"Unexpected property name: {propertyName}");
                }
            return result;
        }

        protected override string GetError(string columnName)
        {
            switch (columnName)
            {
                case "Name":
                    if (string.IsNullOrWhiteSpace(Name))
                        return "Имя участника не может быть пустым";
                    if (Regex.IsMatch(Name, "[^А-Яа-яЁё]"))
                        return "В имени участника не допускаются другие символы, кроме кириллицы";
                    if (Name.Length > 15 || Name.Length < 2)
                        return "Длинна имени участника должна быть от 2 до 15 символа";
                    return "";
                case "Surname":
                    if (string.IsNullOrWhiteSpace(Surname))
                        return "Фамилия участника не может быть пустым";
                    if (Regex.IsMatch(Surname, "[^А-Яа-яЁё]"))
                        return "В фамилии участника не допускаются другие символы, кроме кириллицы";
                    if (Surname.Length > 15 || Surname.Length < 2)
                        return "Длинна фамилии участника должна быть от 2 до 15 символа";
                    return "";
                case "Patronymic":
                    if (Patronymic != null && Regex.IsMatch(Patronymic, "[^А-Яа-яЁё]"))
                        return "В отчестве участника не допускаются другие символы, кроме кириллицы";
                    if (Patronymic.Length > 35)
                        return "Длинна отчества участника должна быть до 35 символа";
                    return "";
                case "Alias":
                    if (Alias?.Length > 35)
                        return "Длинна псевданима участника должна быть до 35 символа";
                    return "";
                case "Sex":
                    if (string.IsNullOrWhiteSpace(Sex))
                        return "Не выбран пол участника";
                    if (sex != "М" && sex != "Ж")
                        return "Неизвестный пол";
                    return "";
                case "YearOfBirth":
                    if (YearOfBirth < 1000)
                        return "Год рождения должен составлять минимум 4 знака";
                    if (YearOfBirth > DateTime.Now.Year)
                        return "Год рождения участника превышает сегодняшную дату";
                    return "";
                case "Height":
                    if (!((height >= 100 || height == 0) && height < 301))
                        return "Росто человека может принимсать значения от 100 до 300 или 0";
                    return "";
                case "Weight":
                    if (!((weight >= 40 || weight == 0) && weight < 301))
                        return "Вес человека может принимсать значения от 40 до 300 или 0";
                    return "";
                case "Club":
                    if (club == null)
                        return "Неуказан клуб";
                    return "";
                case "City":
                    if (city == null)
                        return "Неуказан город";
                    return "";
                case "Groups":
                    if (groups?.Count == 0)
                        return "Количество групп должно превышать 1";
                    return "";
                default:
                    return "";
            }
        }
    }
}