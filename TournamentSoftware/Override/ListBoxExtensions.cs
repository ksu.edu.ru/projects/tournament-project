﻿using System.Windows;
using System.Windows.Controls;
using System.Collections;
using System.Windows.Controls.Primitives;

namespace TournamentSoftware.Override
{
    public class ListBoxExtensions
    {
        static SelectedItemsBinder GetSelectedItemBinder(DependencyObject obj)
        {
            return (SelectedItemsBinder)obj.GetValue(SelectedItemBinderProperty);
        }

        static void SetSelectedItemBinder(DependencyObject obj, SelectedItemsBinder items)
        {
            obj.SetValue(SelectedItemBinderProperty, items);
        }

        static readonly DependencyProperty SelectedItemBinderProperty = DependencyProperty.RegisterAttached("SelectedValueBinder", typeof(SelectedItemsBinder), typeof(ListBoxExtensions));


        public static readonly DependencyProperty SelectedItemsProperty = DependencyProperty.RegisterAttached("SelectedItems", typeof(IList), typeof(ListBoxExtensions),
            new FrameworkPropertyMetadata(null, OnSelectedItemsChanged));


        static void OnSelectedItemsChanged(DependencyObject o, DependencyPropertyChangedEventArgs value)
        {
            var oldBinder = GetSelectedItemBinder(o);
            if (oldBinder != null)
                oldBinder.UnBind();

            SetSelectedItemBinder(o, new SelectedItemsBinder((ListBox)o, (IList)value.NewValue));
            GetSelectedItemBinder(o).Bind();
        }

        public static void SetSelectedItems(Selector elementName, IEnumerable value)
        {
            elementName.SetValue(SelectedItemsProperty, value);
        }

        public static IEnumerable GetSelectedItems(Selector elementName)
        {
            return (IEnumerable)elementName.GetValue(SelectedItemsProperty);
        }
    }
}
