﻿using System.Collections;
using System.Collections.Specialized;
using System.Windows.Controls;
using TournamentSoftware.Infrastructure;
using TournamentSoftware.Models;

namespace TournamentSoftware.Override
{
    public class SelectedItemsBinder
    {
        ListBox _listBox;
        IList _collection;

        public SelectedItemsBinder(ListBox listBox, IList collection)
        {
            _listBox = listBox;
            _collection = collection;

            _listBox.SelectedItems.Clear();

            if (_collection != null)
                foreach (var item in _collection)
                _listBox.SelectedItems.Add(item);
        }

        public void Bind()
        {
            _listBox.SelectionChanged += ListView_SelectionChanged;

            if (_collection is INotifyCollectionChanged)
            {
                var observable = (INotifyCollectionChanged)_collection;
                observable.CollectionChanged += Collection_CollectionChanged;
            }
        }

        public void UnBind()
        {
            if (_listBox != null)
                _listBox.SelectionChanged -= ListView_SelectionChanged;

            if (_collection != null && _collection is INotifyCollectionChanged)
            {
                var observable = (INotifyCollectionChanged)_collection;
                observable.CollectionChanged -= Collection_CollectionChanged;
            }
        }

        void Collection_CollectionChanged(object sender, NotifyCollectionChangedEventArgs e)
        {
            switch (e.Action)
            {
                case NotifyCollectionChangedAction.Add:
                    foreach (var item in e.NewItems ?? new object[0])
                    {
                        if (!_listBox.SelectedItems.Contains(item))
                            _listBox.SelectedItems.Add(item);
                    }
                    break;
                case NotifyCollectionChangedAction.Remove:
                    foreach (var item in e.OldItems ?? new object[0])
                        _listBox.SelectedItems.Remove(item);
                    break;
            }
        }

        void ListView_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            foreach (var item in e.AddedItems ?? new object[0])
            {
                if (!_collection.Contains(item))
                    _collection.Add(item);
            }

            foreach (var item in e.RemovedItems ?? new object[0])
            {
                _collection.Remove(item);
            }
        }
    }
}
