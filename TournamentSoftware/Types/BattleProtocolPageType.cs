﻿namespace TournamentSoftware.Types
{
    enum BattleProtocolPageType
    {
        RoundsData,
        TotalData
    }
}
