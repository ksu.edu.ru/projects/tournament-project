﻿namespace TournamentSoftware.Types
{
    enum MainPageType
    {
        Start,
        MemberRegistration,
        SubgroupsFormation,
        TournamentGrid
    }
}
