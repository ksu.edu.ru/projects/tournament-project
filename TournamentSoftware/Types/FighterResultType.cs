﻿namespace TournamentSoftware.Types
{
    public enum FighterResultType
    {
        Nope,
        Winner,
        DeadHeat,
        Defeat
    }
}
