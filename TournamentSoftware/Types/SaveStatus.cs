﻿namespace TournamentSoftware.Types
{
    enum SaveStatus
    {
        New,
        Saved,
        Reopen
    }
}
