﻿namespace TournamentSoftware.Interfaces
{
    interface IDublicatedObject
    {
        bool IsDuplicated { get; }
    }
}
