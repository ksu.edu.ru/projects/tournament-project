﻿namespace TournamentSoftware.Interfaces
{
    interface IValidObject
    {
        bool IsValid { get; }
    }
}
