﻿using TournamentSoftware.Types;

namespace TournamentSoftware.Interfaces
{
    interface IGetterSaveStatus
    {
        SaveStatus Status { get; }
    }
}
