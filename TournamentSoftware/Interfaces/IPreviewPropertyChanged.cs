﻿using TournamentSoftware.Infrastructure.Events;

namespace TournamentSoftware.Interfaces
{
	interface IPreviewPropertyChanged
	{
		event PreviewPropertyChangedEventHandler PreviewPropertyChanged;
	}
}
