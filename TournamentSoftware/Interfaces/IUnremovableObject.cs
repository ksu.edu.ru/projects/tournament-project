﻿namespace TournamentSoftware.Interfaces
{
    interface IUnremovableObject
    {
        bool IsUnremovable { get; set; }
    }
}
