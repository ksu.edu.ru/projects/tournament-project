﻿namespace TournamentSoftware.Interfaces
{
    interface IEquatableProps<T>
    {
        bool EqualProp(T other, string propertyName = null);
    }
}
