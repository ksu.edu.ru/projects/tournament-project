﻿namespace TournamentSoftware.Interfaces
{
    interface ISelectObject
    {
        bool IsSelected { get; set; }
    }
}
