﻿namespace TournamentSoftware.Interfaces
{
    interface IValidatorObject
    {
        bool CheckValid();
    }
}
