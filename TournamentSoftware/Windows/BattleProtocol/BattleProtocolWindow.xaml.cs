using System.ComponentModel;
using System.Windows;
using TournamentSoftware.ViewModels;
using System.Windows.Navigation;
using System.Windows.Controls;
using System.Text.RegularExpressions;
using System.Windows.Input;

namespace TournamentSoftware.Windows.BattleProtocol
{
    public partial class BattleProtocolWindow : Window
    {
        public BattleProtocolWindow() => InitializeComponent();

        void Window_Closing(object sender, CancelEventArgs e)
        {
            BattleProtocolViewModel battleProtocolViewModel = DataContext as BattleProtocolViewModel;
            if (battleProtocolViewModel.Status == Types.SaveStatus.New)
            {
                MessageBoxResult warningMessageBox = MessageBox.Show("Внесенные вами данные не сохранятся", "Вы действительно хотите выйти?", MessageBoxButton.OKCancel);
                if (warningMessageBox == MessageBoxResult.Cancel)
                    e.Cancel = true;
            }
            DialogResult = battleProtocolViewModel.Status == Types.SaveStatus.Saved;
        }
        
        void frame_Navigating(object sender, NavigatingCancelEventArgs e)
        {
            if (e.NavigationMode != NavigationMode.New)
                e.Cancel = true;

            FrameworkElement content = e.Content as FrameworkElement;
            if (content != null && content.DataContext == null)
                content.DataContext = (sender as Frame).DataContext;
        }

        void customBattleProtocolNumberTextBox_PreviewTextInput(object sender, TextCompositionEventArgs e) => e.Handled = Regex.IsMatch(e.Text, "[^0-9]+");
    }
}