﻿using System.Windows.Controls;
using System.Text.RegularExpressions;
using TournamentSoftware.Models;

namespace TournamentSoftware.Windows.BattleProtocol.Pages
{
    public partial class PreliminaryResultPage : Page
    {
        public PreliminaryResultPage() => InitializeComponent();

        void noteRemarkTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            Fighter fighter = textBox.DataContext as Fighter;
        }

        void totalScoreTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;

            if (textBox.Text.Length <= 0)
                (textBox.DataContext as Fighter).TotalScore = 0;
        }

        void noteRemarkTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (!Regex.IsMatch(e.Text, "[0-9a-zA-Zа-яА-Яа-ёЁ\"_ ]"))
                e.Handled = true;
        }

        void totalScoreTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e)
        {
            if (!Regex.IsMatch(e.Text, "[0-9]"))
                e.Handled = true;
        }
    }
}