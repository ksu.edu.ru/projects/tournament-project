﻿using System.Text.RegularExpressions;
using System.Windows.Controls;
using TournamentSoftware.Models;

namespace TournamentSoftware.Windows.BattleProtocol.Pages
{
    public partial class RoundTablePage : Page
    {
        public RoundTablePage() => InitializeComponent();

        void сountOfWarningsCountOfKnockdownsPointsTextBox_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e) => e.Handled =  !Regex.IsMatch(e.Text, "[0-9]");

        void totalScore_PreviewTextInput(object sender, System.Windows.Input.TextCompositionEventArgs e) => e.Handled = !Regex.IsMatch(e.Text, "[0-9]");

        void totalScore_TextChanged(object sender, TextChangedEventArgs e)
        {
            TextBox textBox = sender as TextBox;
            if (textBox.Text.Length == 0)
                (textBox.DataContext as RoundFighter).TotalScore = 0;
        }
    }
}
