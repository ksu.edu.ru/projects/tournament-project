﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace TournamentSoftware.Windows.Main.Pages
{
    public partial class SubgroupPage : Page
    {
        public SubgroupPage() => InitializeComponent();

        void countOfSubgroupsTextBox_OnPaste(object sender, DataObjectPastingEventArgs e)
        {
            if (!e.SourceDataObject.GetDataPresent(DataFormats.UnicodeText, true))
                return;

            string pastText = e.SourceDataObject.GetData(DataFormats.UnicodeText) as string;
            if (!int.TryParse(pastText, out int _))
            {
                e.CancelCommand();
            }
        }

        void editNameButton_Click(object sender, RoutedEventArgs e)
        {
            int index = getListBoxItemContainerIndex((DependencyObject)e.OriginalSource, subgroupsListBox);

            TextBox textbox = FindFirstParentElement<TextBox>(subgroupsListBox.ItemContainerGenerator.ContainerFromIndex(index));

            textbox.IsEnabled = true;
            textbox.Focus();
            textbox.CaretIndex = textbox.Text.Length;
        }

        static int getListBoxItemContainerIndex(DependencyObject dep, ListBox listBox)
        {
            while ((dep != null) && !(dep is ListBoxItem))
                dep = VisualTreeHelper.GetParent(dep);

            if (dep == null)
                return 0;

            return listBox.ItemContainerGenerator.IndexFromContainer(dep);
        }

        public static T FindFirstParentElement<T>(DependencyObject depObj) where T : DependencyObject
        {
            if (depObj != null)
                for (int i = 0; i < VisualTreeHelper.GetChildrenCount(depObj); i++)
                {
                    DependencyObject child = VisualTreeHelper.GetChild(depObj, i);
                    if (child != null && child is T)
                        return (T)child;

                    T childItem = FindFirstParentElement<T>(child);
                    if (childItem != null)
                        return childItem;
                }

            return null;
        }

        void subgroupNameTextBox_LostFocus(object sender, RoutedEventArgs e) => (sender as TextBox).IsEnabled = false;
    }
}