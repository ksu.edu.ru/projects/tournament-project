using System.Windows;
using System.Windows.Controls;

namespace TournamentSoftware.Windows.Main.Pages
{
    public partial class StartPage : Page
    {
        public StartPage() => InitializeComponent();

        void goToCreateTournamentButton_Click(object sender, RoutedEventArgs e) => tournamentNameTextBox.Focus();
    }
}

