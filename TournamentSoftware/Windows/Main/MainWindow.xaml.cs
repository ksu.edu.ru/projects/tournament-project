﻿using System.Windows;
using System.Windows.Navigation;

namespace TournamentSoftware.Windows.Main
{
    public partial class MainWindow : Window
    {
        public MainWindow() => InitializeComponent();

        void frame_Navigating(object sender, NavigatingCancelEventArgs e) => e.Cancel = e.NavigationMode != NavigationMode.New;
    }
}