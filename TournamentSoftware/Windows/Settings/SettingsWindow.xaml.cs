﻿using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using TournamentSoftware.Interfaces;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels;

namespace TournamentSoftware.Windows.Settings
{
    public partial class SettingsWindow : Window
    {
        public SettingsWindow() => InitializeComponent();

        void Window_Closing(object sender, CancelEventArgs e)
        {
            bool isSaved = false;
            bool isChanged = false;
            foreach (TabItem item in settingsElementTabControl.Items)
            {
                if (item.DataContext is IGetterSaveStatus getterSave)
                    isSaved = isSaved || getterSave.Status == SaveStatus.Saved;
                if (item.DataContext is IChangeTracking changeTracking)
                    isSaved = isSaved || changeTracking.IsChanged;
            }

            if (isChanged && MessageBox.Show("Внесенные вами данные не сохранятся", "Вы действительно хотите выйти?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
            {
                e.Cancel = true;
                return;
            }
            DialogResult = isSaved;
        }

        void settingsElementTabControl_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (e.Source is TabControl && (e.AddedItems[0] as TabItem).DataContext is ClubViewModel && (cityTab.DataContext as CityViewModel).Status == SaveStatus.Saved)
                ((e.AddedItems[0] as TabItem).DataContext as ClubViewModel).UpdateCities();
        }
    }
}