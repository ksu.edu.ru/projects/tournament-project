﻿using System.ComponentModel;
using System.Windows;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels;

namespace TournamentSoftware.Windows.Statistic
{
    public partial class StatisticWindow : Window
    {
        public StatisticWindow() => InitializeComponent();

        void Window_Closing(object sender, CancelEventArgs e) 
        {
            StatisticViewModel viewModel = DataContext as StatisticViewModel;
            if (viewModel.IsChanged && viewModel.Status == SaveStatus.New && MessageBox.Show("Внесенные вами данные не сохранятся.", "Вы действительно хотите выйти?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                e.Cancel = true;
            DialogResult = viewModel.Status == SaveStatus.Saved;
        } 
    }
}
