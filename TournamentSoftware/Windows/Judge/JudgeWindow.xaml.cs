﻿using System.ComponentModel;
using System.Windows;
using TournamentSoftware.Types;
using TournamentSoftware.ViewModels;

namespace TournamentSoftware.Windows.Judge
{
    public partial class JudgeWindow : Window
    {
        public JudgeWindow() => InitializeComponent();

        void Window_Closing(object sender, CancelEventArgs e) 
        {
            JudgeViewModel viewModel = DataContext as JudgeViewModel;
            if (viewModel.IsChanged && viewModel.Status == SaveStatus.New && MessageBox.Show("Внесенные вами данные не сохранятся.", "Вы действительно хотите выйти?", MessageBoxButton.OKCancel) == MessageBoxResult.Cancel)
                e.Cancel = true;
            DialogResult = viewModel.Status == SaveStatus.Saved;
        } 
    }
}