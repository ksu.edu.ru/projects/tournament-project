﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace TournamentSoftware.Converters
{
    class ToArrayConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture) => values.Clone();

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture) => new object[1] { Binding.DoNothing };
    }
}
