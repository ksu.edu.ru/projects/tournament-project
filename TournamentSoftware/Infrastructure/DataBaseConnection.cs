﻿using SQLite;
using System.Collections.Generic;
using TournamentSoftware.Models;

namespace TournamentSoftware.Infrastructure
{
    static class DataBaseConnection
    {
        static DataBaseConnection()
        {
            SQLiteConnection = new SQLiteConnection("db.db");
            CreateTables();
            FillGroupRules();
            FillFightSystem();
        }

        public static SQLiteConnection SQLiteConnection { get; set; }

        static void CreateTables()
        {
            SQLiteConnection.CreateTable<Club>();
            SQLiteConnection.CreateTable<Member>();
            SQLiteConnection.CreateTable<Tournament>();
            SQLiteConnection.CreateTable<Nomination>();
            SQLiteConnection.CreateTable<TournamentNomination>();
            SQLiteConnection.CreateTable<Category>();
            SQLiteConnection.CreateTable<TournamentCategory>();
            SQLiteConnection.CreateTable<GroupRule>();
            SQLiteConnection.CreateTable<Group>();
            SQLiteConnection.CreateTable<GroupRule_Group>();
            SQLiteConnection.CreateTable<GroupMember>();
            SQLiteConnection.CreateTable<Subgroup>();
            SQLiteConnection.CreateTable<TournamentJudge>();
            SQLiteConnection.CreateTable<Judge>();
            SQLiteConnection.CreateTable<FightSystem>();
            SQLiteConnection.CreateTable<SubgroupMember>();
            SQLiteConnection.CreateTable<Phase>();
            SQLiteConnection.CreateTable<BattleProtocol>();
            SQLiteConnection.CreateTable<JudgeNote>();
            SQLiteConnection.CreateTable<Round>();
            SQLiteConnection.CreateTable<RoundFighter>();
            SQLiteConnection.CreateTable<Fighter>();
            SQLiteConnection.CreateTable<City>();
        }

        static void FillGroupRules()
        {
            List<GroupRule> groupRules = new List<GroupRule>();
            new List<string> { "Правило посевных бойцов", "Правило одноклубников", "Правило города" }.ForEach(rule =>
            {
                if (SQLiteConnection.Find<GroupRule>(r => r.Name == rule) == null)
                    groupRules.Add(new GroupRule
                    {
                        Name = rule
                    });
            });
            SQLiteConnection.InsertAll(groupRules);
        }

        static void FillFightSystem()
        {
            List<FightSystem> fightSystems = new List<FightSystem>();
            new List<string> { "Круговая", "На вылет", "До двух поражений" }.ForEach(fightSystem =>
            {
                if (SQLiteConnection.Find<FightSystem>(fs => fs.Name == fightSystem) == null)
                    fightSystems.Add(new FightSystem
                    {
                        Name = fightSystem
                    });
            });
            SQLiteConnection.InsertAll(fightSystems);
        }

        public static void UpdateTournament(Tournament tournament) => SQLiteConnection.Update(tournament);
    }
}
