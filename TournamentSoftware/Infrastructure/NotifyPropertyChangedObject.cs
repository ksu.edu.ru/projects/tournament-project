﻿using System.ComponentModel;
using System.Runtime.CompilerServices;
using TournamentSoftware.Infrastructure.Events;
using TournamentSoftware.Infrastructure.Events.EventArgs;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Infrastructure
{
    public abstract class NotifyPropertyChangedObject : INotifyPropertyChanged, IPreviewPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public event PreviewPropertyChangedEventHandler PreviewPropertyChanged;

        protected virtual void OnPropertyChanged([CallerMemberName] string propetyName = null) => PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propetyName));

        protected virtual bool Set<T>(ref T field, T value, [CallerMemberName] string propetyName = null)
        {
            if (Equals(field, value))
                return false;
            field = value;
            OnPropertyChanged(propetyName);
            return true;
        }

        protected virtual bool PreviewSet<T>(ref T field, T value, [CallerMemberName] string propetyName = null)
        {
            PreviewPropertyChangedEventArgs eventArgs = new PreviewPropertyChangedEventArgs(propetyName, field, value);
            PreviewPropertyChanged?.Invoke(this, eventArgs);
            if (!eventArgs.Handled)
                return Set(ref field, value);
            return false;
        }
    }
}