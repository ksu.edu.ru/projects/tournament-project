﻿using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;

namespace TournamentSoftware.Infrastructure
{
    public abstract class ValidateModel : NotifyPropertyChangedObject, IDataErrorInfo
    {
        Dictionary<string, string> propertyToError;

        public ValidateModel()
        {
            propertyToError = new Dictionary<string, string>();
        }

        public string this[string columnName]
        {
            get
            {
                string error = GetError(columnName);
                if (!string.IsNullOrWhiteSpace(error))
                    if (propertyToError.ContainsKey(columnName))
                        propertyToError[columnName] = error;
                    else
                        propertyToError.Add(columnName, error);
                else
                    propertyToError.Remove(columnName);
                OnPropertyChanged("Error");
                return error;
            }
        }

        public virtual string Error
        {
            get => propertyToError.Values.FirstOrDefault();
        }

        protected abstract string GetError(string columnName);
    }
}
