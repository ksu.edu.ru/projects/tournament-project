using System;
using System.ComponentModel;
using TournamentSoftware.Interfaces;

namespace TournamentSoftware.Infrastructure
{
    class EditableModel<TModel> : NotifyPropertyChangedObject, IChangeTracking, ISelectObject, IUnremovableObject, IValidObject, IDublicatedObject where TModel : INotifyPropertyChanged, IDataErrorInfo, ICloneable, IEquatableProps<TModel>
    {
        TModel data;
        TModel backup;
        bool isChanged;
        bool isSelected;
        bool isUnremovable;
        bool isDublicated;
        bool isValid;

        public EditableModel(TModel data, bool isNew = false)
        {
            this.data = data;
            this.data.PropertyChanged += Data_PropertyChanged;
            if (isNew)
                isChanged = true;
            else
                CloneData();
            CheckIsValid();
        }

        public TModel Data => data;

        public TModel Backup => backup;

        public bool IsNew => backup == null;

        public Func<TModel, bool> ChangedOutSide { get; set; }

        public bool IsChanged
        {
            get => isChanged;
            private set
            {
                isChanged = value;
                OnPropertyChanged();
            }
        }

        public bool IsUnremovable
        {
            get => isUnremovable;
            set => Set(ref isUnremovable, value);
        }

        public bool IsSelected
        {
            get => isSelected;
            set => Set(ref isSelected, value);
        }

        public bool IsValid
        {
            get => isValid;
            private set => Set(ref isValid, value);
        }

        public bool IsDuplicated
        {
            get => isDublicated;
            set => Set(ref isDublicated, value);
        }

        void Data_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            CheckIsChanged();
            CheckIsValid();
        }

        public void CheckIsChanged() => IsChanged = !data.EqualProp(backup) || (ChangedOutSide?.Invoke(data) ?? false);

        public bool CheckIsPropertyChanged(string propertyName) => !data.EqualProp(backup, propertyName);

        public void CheckIsValid() => IsValid = string.IsNullOrWhiteSpace(data.Error);

        public void AcceptChanges()
        {
            CloneData();
            CheckIsChanged();
        }

        void CloneData() => backup = (TModel)data.Clone();
    }
}
