﻿using TournamentSoftware.Infrastructure.Events.EventArgs;

namespace TournamentSoftware.Infrastructure.Events
{
    public delegate void PreviewPropertyChangedEventHandler(object sender, PreviewPropertyChangedEventArgs e);
}
