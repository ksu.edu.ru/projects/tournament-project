﻿using System.ComponentModel;

namespace TournamentSoftware.Infrastructure.Events.EventArgs
{
    public class PreviewPropertyChangedEventArgs : PropertyChangedEventArgs
	{
		public bool Handled { get; set; }
		public object NewValue { get; set; }
		public object OldValue { get; set; }

		public PreviewPropertyChangedEventArgs(string propertyName, object oldValue, object newValue) : base(propertyName)
		{
			OldValue = oldValue;
			NewValue = newValue;
		}

	}
}
